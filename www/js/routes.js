angular.module('starter.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      })

      .state('email_login', {
        url: '/email_login',
        templateUrl: 'templates/email_login.html',
        controller: 'Email_LoginCtrl'
      })

      .state('sign_up', {
        url: '/sign_up',
        templateUrl: 'templates/sign_up.html',
        controller: 'SignUpCtrl'
      })

      // setup an abstract state for the tabs directive
      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
      })

      // Each tab has its own nav history stack:

      .state('tab.cols', {
        url: '/cols',
        views: {
          'tab-cols': {
            templateUrl: 'templates/cols.html',
            controller: 'ColsCtrl'
          }
        }
      })
      // .state('tab.choosemember', {
      //   url: '/choosemember',
      //   views: {
      //     'tab-deuxmillecols': {
      //       templateUrl: 'templates/choosemember.html',
      //       controller: 'ChooseMemberCtrl'
      //     }
      //   }
      // })

      .state('tab.shop', {
        url: '/shop',
        cache: false,
        views: {
          'tab-shop': {
            templateUrl: 'templates/shop.html',
            controller: 'ShopCtrl'
          }
        }
      })

      .state('tab.feed', {
        url: '/feed',
        views: {
          'tab-feed': {
            templateUrl: 'templates/feed.html',
            controller: 'FeedCtrl'
          }
        }
      })

      .state('feed_item', {
        url: '/feed_item',
        cache: false,
            templateUrl: 'templates/feed_item.html',
            controller: 'FeedItemCtrl'
      })

      .state('tab.profile', {
        url: '/profile',
        cache: false,
        views: {
          'tab-profile': {
            templateUrl: 'templates/profile.html',
            controller: 'ProfileCtrl'
          }
        },
        params: {
          'user_id': 'empty',
          'ftp': 'null',
          'weight': 'null',
          'bike_weight': 'null',
          'avatar': 'null',
          'displayname': 'null'
        }
      })

      .state('profile_more', {
        url: '/profile_more',
        cache: false,
          templateUrl: 'templates/profile_more.html',
          controller: 'ProfileMoreCtrl'
      })

      .state('profile_settings', {
        url: '/profile_settings',
        cache: false,
          templateUrl: 'templates/profile_settings.html',
          controller: 'ProfileSettingsCtrl'
      })

      .state('profile_settings_notification', {
        url: '/profile_settings_notification',
        cache: false,
          templateUrl: 'templates/profile_settings_notification.html',
          controller: 'ProfileSettingsNotificationCtrl'
      })

      .state('tab.usercolslist', {
        url: '/usercolslist',
        cache: false,
        views: {
          'tab-profile': {
            templateUrl: 'templates/usercols_list.html',
            controller: 'UserColsCtrl'
          }
        }
      })

      .state('album', {
        url: '/album',
        cache: false,
        templateUrl: 'templates/album.html',
        controller: 'AlbumCtrl',
        params: {
          'isNewPost': false,
        }
      })

      .state('notifications', {
        url: '/notifications',
        cache: false,
        templateUrl: 'templates/notifications.html',
        controller: 'NotificationsCtrl'
      })

      .state('col_detail', {
        url: '/col_detail',
        cache: false,
        templateUrl: 'templates/col_detail.html',
        controller: 'ColDetailCtrl',
        params: {
          'col_id': 'some default',
          'isChecked': 'some default'
        }
      })

      .state('save_col', {
        url: '/save_col',
        cache: false,
        templateUrl: 'templates/save_col.html',
        controller: 'SaveColCtrl',
        params: {
          'col_id': 'some default',
          'segments': 'some default',
          'col_name': 'some default'
        }
      })

      .state('all_cols_page', {
        url: '/all_cols_page',
        templateUrl: 'templates/all_cols_page.html',
        controller: 'AllColsPageCtrl'
      })

      .state('product_description', {
        url: '/product_description',
        cache: false,
        templateUrl: 'templates/product.html',
        controller: 'ProductDescCtrl',
        params: {
          'product': 'some default'
        }
      });

// if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

  });
