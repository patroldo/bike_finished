//var profile_data;
angular.module('starter.controllers', ['ionic', 'ngCordova', 'ngCordovaOauth'])
.directive('compile', ['$compile', function ($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        // watch the 'compile' expression for changes
        return scope.$eval(attrs.compile);
      },
      function(value) {
        // when the 'compile' expression changes
        // assign it into the current DOM
        element.html(value);

        // compile the new DOM and link it to the current
        // scope.
        // NOTE: we only compile .childNodes so that
        // we don't get into infinite loop compiling ourselves
        $compile(element.contents())(scope);
      }
  );
};
}])

// here functions that are needed globally are defined
  .run(function ($rootScope, $cordovaInAppBrowser, $ionicHistory, homeFactory) {
    $rootScope.my_activities = [];

    $rootScope.formatIntegerToTwoDigits = function (val) {
      return val < 10 ? '0' + val.toString() : val.toString();
    }
    // function that loads profile data after login
    $rootScope.loadProfileData = function ($state) {
      var link = 'https://www.deuxmille.cc/api/user.get_memberinfo/?user_id=' + $rootScope.userID.toString() + '&insecure=cool';
      //Console.log(link);
      homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          var result = response.data;
          if (result.status == "ok") {
            $rootScope.profileResults = result;
            if (result != null) {
              $rootScope.profile_data = {};
              $rootScope.profile_data.avatar = result.avatar;
              $rootScope.profile_data.strava_sync = result.strava_sync;
              $rootScope.profile_data.units = result.units != null ? result.units : '';
              $rootScope.profile_data.ftp = result.ftp != null ? result.ftp : '';
              $rootScope.profile_data.weight = result.weight != null ? result.weight : '';
              $rootScope.profile_data.bikeweight = result.bike_weight != null ? result.bike_weight : '';
              $rootScope.profile_data.pwr = (result.ftp != null && result.weight != null) ? Math.round(parseFloat(result.ftp) / parseFloat(result.weight) * 10) / 10 : '';
              $rootScope.profile_data.name = result.name;
              $rootScope.profile_data.displayname = result.displayname;
              $rootScope.profile_data.country = result.country;
              if((result.user_col_level == '') || (result.user_col_level == null))
                $rootScope.profile_data.user_col_level = 'No Cat';
              else
                $rootScope.profile_data.user_col_level = result.user_col_level;
              //$rootScope.checkProfileData($ionicModal);
            }
          } else {
            navigator.notification.alert('No profile data!', function () {
            }, 'Error');
          }
          $rootScope.IsAfterLogin = true;
          $rootScope.NumDiv = -1;
      $rootScope.isNewPost_Add = {};
          //$state.go('tab.feed');
          if(!$rootScope.profile_data.strava_sync)
            $rootScope.synchronizeWithStrava();
          $ionicHistory.clearCache(['tab.feed', 'tab.cols', 'all_cols_page']).then(function(){
          $ionicHistory.clearHistory();
          $state.go('tab.feed');
            //$state.go('save_col');
          });
        });
    }
    
    $rootScope.synchronizeWithStrava = function () {
      //NativeStorage.getItem('strava_synchronization_' + $rootScope.userID.toString(), function (val) {
        //if (val) {
          /*//var linkTokenSynchronize = 'https://www.deuxmille.cc/api/user/update_user_strava/?insecure=cool&user_id=' + $rootScope.userID.toString();
          homeFactory.apiCall(linkTokenSynchronize, 'GET').then(function (response) {
            var result = response.data;
            //Console.log("STRAVA "+JSON.stringify(result));
            if (result.status == 'success') {
              navigator.notification.alert('Your data was successfully synchronized.', function () {
              }, 'Key data');
            } else {
              navigator.notification.alert('Error happened during synchronization.', function () {
              }, 'Error');
            }
          });*/
        //}
      //}, function () {
        navigator.notification.confirm('Do you want to synchronize data with Strava?', function (buttonIndex) {
          if (buttonIndex == 1) {
            $rootScope.ConnectToStravaNEW(false);
            //NativeStorage.setItem('strava_synchronization_' + $rootScope.userID.toString(), true, function (success) {
              //Console.log("Successful setting into storage.");
            //}, function (error) {
              //Console.log("Error while setting into storage.");
            //});
          //} else {
            //NativeStorage.setItem('strava_synchronization_' + $rootScope.userID.toString(), false, function (success) {
              //Console.log("Successful setting into storage.");
            //}, function (error) {
              //Console.log("Error while setting into storage.");
            //});
          }
        }, 'Key data', ['OK', 'Cancel']);
      //});
    }

    // Newton-Raphson algorithm implementation
    $rootScope.NewtonRaphson = function (a, c, e, d) {
      var vi = 20;
      var i = 1;
      var converged = false;
      var fvel, fdvel, dv, cct;
      while (i < 100 && converged == false) {
        fvel = a * vi * vi * vi + 2 * a * e * vi * vi + (a * e * e + c) * vi + d;
        fdvel = 3 * a * vi * vi + 4 * a * e * vi + a * e * e + c;
        dv = fvel / fdvel;

        cct = Math.abs(dv / vi);
        vi -= dv;
        if (cct < 0.001) {
          converged = true;
        }
        i += 1;
      }

      if (converged == false) {
        return 0;
      } else {
        return vi * 3.6;
      }
    }

    $rootScope.recalculate = function (gradient, ftp, length, weight) {
      var A = 0.509; //Frontal area
      var Cd = 0.63; //Drag coefficient
      var dt = 3; //Drivetrain loss
      var Crr = 0.005; //Rolling resistance coefficient
      var rho = 1.056; //Air Density
      var g = 9.8067; //Gravity
      var G = parseFloat(gradient);
      var FG = g * Math.sin(Math.atan(G / 100)) * weight; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * weight * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;
      var c = FG + FRR;

      //Method 2 - To calculate Velocity
      var P = parseFloat(ftp) * 0.9;
      var vh = 0;
      var D = length;
      var d = -P * ((1 - (dt / 100)));
      var e = vh / 3.6;

      var V = $rootScope.NewtonRaphson(a, c, e, d);
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V;
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time
      var time = null;
      if (V != 0) {
        time = T;
      } else {
        time = 'undefined';
      }
      return {
        speed: V,
        time: time
      };
    }

    
  })

  .controller('LoginCtrl', function ($scope, $state, $ionicModal, $rootScope, homeFactory) {

    NativeStorage.getItem('user_auth', function (val) {
      if (val.id && val.cookie) {
        $rootScope.isFacebookLogin = val.facebook_login;
        $rootScope.userID = val.id;
        $rootScope.authCookie = val.cookie;
        $rootScope.loadProfileData($state);
        //$rootScope.synchronizeWithStrava();
        $rootScope.IsAfterLogin = true;
        $rootScope.NumDiv = -1;
      $rootScope.isNewPost_Add = {};
        //$state.go('profile_settings');
      }
    }, function () {
    });

    $scope.login = function () {
      //Console.log("login clicked.");
      $state.go('email_login');
      //$state.go('save_col');
    }

    $scope.signup = function () {
      $state.go('sign_up');
    }

  })

  .controller('Email_LoginCtrl', function ($scope, $rootScope, $state, $cordovaOauth, $ionicModal, $cordovaFacebook, homeFactory) {
    $rootScope.isFacebookLogin = false;
    var indexHeight = $(window).height();
    if (indexHeight > 620) {
      $('.login_block').css('height', indexHeight);
    } else {
      $('.login_block').css('height', 640);
    }

    $scope.user = {
      email: "",
      password: ""
    };

    NativeStorage.getItem('loginCredentials', function (val) {
      $scope.user.email = val.email;
      $scope.user.password = val.password;
    }, function () {
    });

    $scope.forgot = function () {
      //Console.log('forgot');
      window.open('https://www.deuxmille.cc/password-reset/');
    };

    $scope.login_facebook = function () {
      //$cordovaOauth.facebook("273083806475263", ["email"]).then(function (result) {
        window.CordovaFacebook.login({
          permissions: ['email'],
          onSuccess: function(result) {
            //Console.log(JSON.stringify(result));
            var access_token_res = result.accessToken;
            var link = 'https://www.deuxmille.cc/api/user/fb_connect/?access_token=' + access_token_res + '&insecure=cool';
            homeFactory
              .facebookCall(link, 'GET')
              .then(function (response) {
                $scope.facebook_res = response.data;
                $rootScope.userID = response.data.wp_user_id;
                $rootScope.authCookie = response.data.cookie;
                NativeStorage.setItem('user_auth', {
                  id: $rootScope.userID,
                  cookie: $rootScope.authCookie,
                  facebook_login: true
                }, function (success) {
                  //Console.log("Successful setting into storage.");
                }, function (error) {
                  //Console.log("Error while setting into storage.");
                });
                $rootScope.loadProfileData($state);
                //$rootScope.synchronizeWithStrava();
                $rootScope.isFacebookLogin = true;
                })
          },
        onFailure: function (result) {
          //Console.log("ERRORRRRRRRRRRR");
          //Console.log(JSON.stringify(result));
        }
      });
    };

    $scope.login = function () {

      //Console.log('email :', $scope.user.email);
      //Console.log('password:', $scope.user.password);

      if ($scope.user.email.length == 0 || $scope.user.password.length == 0) {
        navigator.notification.alert('Email or password is not specified.', function() {}, 'Error');
      } else {

        $rootScope.show();

        var link1 = 'https://www.deuxmille.cc/api/get_nonce/?controller=user&method=generate_auth_cookie';
        homeFactory
          .apiCall(link1, 'GET')
          .then(function (response) {
            $rootScope.hide();
            //Console.log(response);
            if (response.data.status == "ok") {

              var link = 'https://www.deuxmille.cc/api/user/generate_auth_cookie/?username=' + $scope.user.email + '&password=' + $scope.user.password + '&insecure=cool';

              homeFactory
                .apiCall(link, 'GET')
                .then(function (response) {
                  var result = response.data;
                  if (result.status == "ok") {
                    $rootScope.userID = result.user.id;
                    $rootScope.authCookie = result.cookie;
                    NativeStorage.setItem('user_auth', {
                      id: $rootScope.userID,
                      cookie: $rootScope.authCookie,
                      facebook_login: false
                    }, function (success) {
                      //Console.log("Successful setting into storage.");
                    }, function (error) {
                      //Console.log("Error while setting into storage.");
                    });
                    $rootScope.loadProfileData($state);
                    //$rootScope.synchronizeWithStrava();
                    NativeStorage.setItem('loginCredentials', {
                      email: $scope.user.email,
                      password: $scope.user.password
                    }, function () {
                    }, function () {
                    });
                  } else {
                    if (result.error) {
                      navigator.notification.alert(result.error, function() {}, 'Error');
                    } else {
                      navigator.notification.alert('User Login failed!', function() {}, 'Error');
                    }
                  }
                });
            }
          });
      }
    };

    $scope.signup = function () {
      $state.go('sign_up');
    };

  })

  .controller('SignUpCtrl', function ($scope, $rootScope, $state, $cordovaOauth, $ionicModal, $cordovaFacebook, homeFactory) {
    var indexHeight = $(window).height();
    if (indexHeight > 620) {
      $('.registration_block').css('height', indexHeight);
      var blockTopPosition = (indexHeight - $('.reg_block_outer').height())/2;
      $('.reg_block_outer').css('top',blockTopPosition);
    } else {
      $('.registration_block').css('height', 640);
      $('.reg_block_outer').css('top', 110);
    }

    $scope.register = {
      user_name: "",
      user_email: "",
      user_password: "",
      user_password_conf: ""
    };

    $scope.register = function() {
      if (!($scope.register.user_name && $scope.register.user_email && $scope.register.user_password
          && $scope.register.user_password_conf)) {
        navigator.notification.alert('Please fill in all fields', function() {}, 'Error');
      } else if ($scope.register.user_password != $scope.register.user_password_conf) {
        navigator.notification.alert('Passwords do not match', function() {}, 'Error');
      } else {
        $rootScope.show();
        var linkNonce = 'https://www.deuxmille.cc/api/get_nonce/?controller=user&method=register&insecure=cool';
        homeFactory
          .apiCall(linkNonce, 'GET')
          .then(function (response) {
            $rootScope.hide();
            var result = response.data;
            if (result.status == 'ok') {
              var nonce = result.nonce;
              var linkRegister = 'https://www.deuxmille.cc/api/user/register/?username='
                + $scope.register.user_name + '&email=' + $scope.register.user_email + '&nonce='
                + nonce + '&notify=both&user_pass=' + $scope.register.user_password
                + '&display_name=' + $scope.register.user_name + '&insecure=cool';
              $rootScope.show();
              homeFactory.apiCall(linkRegister, 'GET').then(function (response2) {
                $rootScope.hide();
                result = response2.data;
                if (result.status = 'ok') {
                  navigator.notification.alert('Registration successful!', function() {}, 'Information');
                  $state.go('email_login');
                }
              })
            } else {
              navigator.notification.alert('User Registration failed!', function() {}, 'Error');
            }
          });
      }
    };

    $scope.login_facebook = function () {
/*      $cordovaOauth.facebook("273083806475263", ["email"]).then(function (result) {
        var access_token_res = result.access_token;
        var link = 'https://www.deuxmille.cc/api/user/fb_connect/?access_token=' + access_token_res + '&insecure=cool';
        homeFactory
          .facebookCall(link, 'GET')
          .then(function (response) {
            $scope.facebook_res = response.data;
            $rootScope.userID = response.data.wp_user_id;
            $rootScope.authCookie = response.data.cookie;
            NativeStorage.setItem('user_auth', {
              id: $rootScope.userID,
              cookie: $rootScope.authCookie,
              facebook_login: true
            }, function (success) {
              //Console.log("Successful setting into storage.");
            }, function (error) {
              //Console.log("Error while setting into storage.");
            });
            $rootScope.loadProfileData(homeFactory, $state, $ionicModal);
            $rootScope.synchronizeWithStrava();
          })
      }, function (error) {
        navigator.notification.alert('Auth Failed..!!' + error, function() {}, 'Error');
      });*/
        window.CordovaFacebook.login({
          permissions: ['email'],
          onSuccess: function(result) {
            //Console.log(JSON.stringify(result));
            var access_token_res = result.accessToken;
            var link = 'https://www.deuxmille.cc/api/user/fb_connect/?access_token=' + access_token_res + '&insecure=cool';
            homeFactory
              .facebookCall(link, 'GET')
              .then(function (response) {
                $scope.facebook_res = response.data;
                $rootScope.userID = response.data.wp_user_id;
                $rootScope.authCookie = response.data.cookie;
                NativeStorage.setItem('user_auth', {
                  id: $rootScope.userID,
                  cookie: $rootScope.authCookie,
                  facebook_login: true
                }, function (success) {
                  //Console.log("Successful setting into storage.");
                }, function (error) {
                  //Console.log("Error while setting into storage.");
                });
                $rootScope.loadProfileData($state);
                //$rootScope.synchronizeWithStrava();
                $rootScope.isFacebookLogin = true;
                })
          },
        onFailure: function (result) {
        navigator.notification.alert('Auth Failed..!!' + error, function() {}, 'Error');
        }
      });
    };
  })

  

  .controller('CalculatorCtrl', function ($scope, $rootScope) {

    //Constant
    var A = 0.509; //Frontal area
    var Cd = 0.63; //Drag coefficient
    var dt = 3; //Drivetrain loss
    var Crr = 0.005; //Rolling resistance coefficient
    var rho = 1.226; //Air Density
    var g = 9.8067; //Gravity

    $scope.data = {};
    $scope.data.timeSelection = 1000;

    var distanceChanging = true;

    $scope.$watch('data.timeSelection', function (newValue, oldValue) {
      if (newValue != null) {
        var hours = $rootScope.formatIntegerToTwoDigits(parseInt($scope.data.timeSelection / 3600));
        var minutes = $rootScope.formatIntegerToTwoDigits(parseInt(($scope.data.timeSelection - hours * 3600) / 60));
        var seconds = $rootScope.formatIntegerToTwoDigits(parseInt($scope.data.timeSelection % 60));
        $scope.data.timeDisplay = hours + ":" + minutes + ":" + seconds;
      } else {
        $scope.data.timeDisplay = 'undefined';
      }
    }, true);

    // click handlers for + or - pressed
    $scope.minusTimeClicked = function () {
      $scope.data.timeSelection = parseFloat($scope.data.timeSelection) - 1;
      $scope.timeChange($scope.data.timeSelection);
    }

    $scope.plusTimeClicked = function () {
      $scope.data.timeSelection = parseFloat($scope.data.timeSelection) + 1;
      $scope.timeChange($scope.data.timeSelection);
    }

    $scope.minusDistanceClicked = function () {
      $scope.data.distanceSelection = parseFloat($scope.data.distanceSelection) - 100;
      $scope.distanceChange($scope.data.distanceSelection);
    }

    $scope.plusDistanceClicked = function () {
      $scope.data.distanceSelection = parseFloat($scope.data.distanceSelection) + 100;
      $scope.distanceChange($scope.data.distanceSelection);
    }

    $scope.minusSpeedClicked = function () {
      $scope.data.speedSelection = parseFloat($scope.data.speedSelection) - 1;
      $scope.speedChange($scope.data.speedSelection);
    }

    $scope.plusSpeedClicked = function () {
      $scope.data.speedSelection = parseFloat($scope.data.speedSelection) + 1;
      $scope.speedChange($scope.data.speedSelection);
    }

    $scope.minusPowerClicked = function () {
      $scope.data.wattsSelection = parseFloat($scope.data.wattsSelection) - 1;
      $scope.powerChange($scope.data.wattsSelection);
    }

    $scope.plusPowerClicked = function () {
      $scope.data.wattsSelection = parseFloat($scope.data.wattsSelection) + 1;
      $scope.powerChange($scope.data.wattsSelection);
    }

    $scope.minusWindClicked = function () {
      $scope.data.windSelection = parseFloat($scope.data.windSelection) - 1;
      $scope.windChange($scope.data.windSelection);
    }

    $scope.plusWindClicked = function () {
      $scope.data.windSelection = parseFloat($scope.data.windSelection) + 1;
      $scope.windChange($scope.data.windSelection);
    }

    $scope.minusGradientClicked = function () {
      $scope.data.gradientSelection = Math.round((parseFloat($scope.data.gradientSelection) - 0.1) * 10) / 10;
      $scope.gradientChange($scope.data.gradientSelection);
    }

    $scope.plusGradientClicked = function () {
      $scope.data.gradientSelection = Math.round((parseFloat($scope.data.gradientSelection) + 0.1) * 10) / 10;
      $scope.gradientChange($scope.data.gradientSelection);
    }

    $scope.minusWeightClicked = function () {
      $scope.data.totalweightSelection = parseFloat($scope.data.totalweightSelection) - 0.5;
      $scope.weightChange($scope.data.totalweightSelection);
    }

    $scope.plusWeightClicked = function () {
      $scope.data.totalweightSelection = parseFloat($scope.data.totalweightSelection) + 0.5;
      $scope.weightChange($scope.data.totalweightSelection);
    }

    $scope.data.distanceSelection = 15000;
    $scope.data.windSelection = 0;
    $scope.data.gradientSelection = 0;

    $scope.$watch('data.distanceSelection', function (newValue) {
      $scope.data.distanceDisplay = Math.round(newValue / 1000 * 10) / 10;
    }, true);

    $scope.data.seaLevelSelection = false;

    $scope.$on("$ionicView.enter", function (event, data) {

      $scope.data.wattsSelection = $rootScope.profile_data.ftp ? $rootScope.profile_data.ftp : 312;
      $scope.data.totalweightSelection = $rootScope.profile_data.weight && $rootScope.profile_data.bikeweight ? parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight) : 83;

      $scope.powerChange($scope.data.wattsSelection);
      $scope.distanceChange($scope.data.distanceSelection);
    });

    // listener for time change
    $scope.timeChange = function (val) {
      if (distanceChanging) {
        var V = parseFloat($scope.data.distanceSelection) / val * 3.6;
        $scope.data.speedSelection = Math.round(V * 10) / 10;
        $scope.speedChange(V);
      } else {
        var V = parseFloat($scope.data.speedSelection);
        $scope.data.distanceSelection = V / 3.6 * parseFloat(val);
      }
    }

    // listener for sea level / mountain toggle change
    $scope.seaLevelToggleChange = function (val) {
      if (val) {
        rho = 1.056;
      } else {
        rho = 1.226;
      }
      $scope.powerChange($scope.data.wattsSelection);
    }

    $scope.speedChange = function (val) {
      //Basic Calculations
      var W = parseFloat($scope.data.totalweightSelection);
      var G = parseFloat($scope.data.gradientSelection);
      var FG = g * Math.sin(Math.atan(G / 100)) * W; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * W * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;

      //Method 1 - To calculate Power
      var V = parseFloat(val);
      var vh = parseFloat($scope.data.windSelection);
      var D = parseFloat($scope.data.distanceSelection);
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V; //Fdrag
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time

      if (val != 0) {
        $scope.data.timeSelection = T;
      } else {
        $scope.data.timeSelection = null;
      }
      $scope.data.wattsSelection = '' + Math.round((1 / (1 - (dt / 100)) * FR * (V / 3.6)) * 10) / 10;
    }

    $scope.powerChange = function (val) {
      distanceChanging = false;
      //Basic Calculations
      var W = parseFloat($scope.data.totalweightSelection);
      var G = parseFloat($scope.data.gradientSelection);
      var FG = g * Math.sin(Math.atan(G / 100)) * W; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * W * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;
      var c = FG + FRR;

      //Method 2 - To calculate Velocity
      var P = parseFloat(val);
      var vh = parseFloat($scope.data.windSelection);
      var D = parseFloat($scope.data.distanceSelection);
      var d = -P * ((1 - (dt / 100)));
      var e = vh / 3.6;

      var V = $rootScope.NewtonRaphson(a, c, e, d);
      $scope.data.speedSelection = '' + Math.round(V * 10) / 10;
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V;
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time

      $scope.data.timeSelection = T;
    }

    $scope.distanceChange = function (val) {
      distanceChanging = true;
      var V = parseFloat($scope.data.speedSelection);
      var D = parseFloat(val);
      var T = 3.6 * D / V; //Time
      $scope.data.timeSelection = T;
    }

    $scope.gradientChange = function (val) {
      var W = parseFloat($scope.data.totalweightSelection);
      var G = parseFloat(val);
      var FG = g * Math.sin(Math.atan(G / 100)) * W; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * W * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;
      var c = FG + FRR;
      var P = parseFloat($scope.data.wattsSelection);
      var vh = parseFloat($scope.data.windSelection);
      var D = parseFloat($scope.data.distanceSelection);
      var d = -P * (1 - (dt / 100));
      var e = vh / 3.6;

      var V = $rootScope.NewtonRaphson(a, c, e, d);
      $scope.data.speedSelection = '' + Math.round(V * 10) / 10;
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V;
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time

      $scope.data.timeSelection = T;
    }

    $scope.windChange = function (val) {
      var W = parseFloat($scope.data.totalweightSelection);
      var G = parseFloat($scope.data.gradientSelection);
      var FG = g * Math.sin(Math.atan(G / 100)) * W; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * W * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;
      //Method 1 - To calculate Power
      var V = parseFloat($scope.data.speedSelection);
      var vh = parseFloat(val);
      var D = parseFloat($scope.data.distanceSelection);
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V; //Fdrag
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time

      $scope.data.timeSelection = T;
      $scope.data.wattsSelection = '' + Math.round((1 / (1 - (dt / 100)) * FR * (V / 3.6)) * 10) / 10;
    }

    $scope.weightChange = function (val) {
      var W = parseFloat(val);
      var G = parseFloat($scope.data.gradientSelection);
      var FG = g * Math.sin(Math.atan(G / 100)) * W; //Fgravity
      var FRR = g * Math.cos(Math.atan(G / 100)) * W * Crr //Frolling-resistance
      var a = 0.5 * A * Cd * rho;
      var b = 0;
      var c = FG + FRR;

      //Method 2 - To calculate Velocity
      var P = parseFloat($scope.data.wattsSelection);
      var vh = parseFloat($scope.data.windSelection);
      var D = parseFloat($scope.data.distanceSelection);
      var d = -P * ((1 - (dt / 100)));
      var e = vh / 3.6;

      var V = $rootScope.NewtonRaphson(a, c, e, d);
      $scope.data.speedSelection = '' + Math.round(V * 10) / 10;
      var temp_V = (V + vh) / 3.6;
      var FD = a * temp_V * temp_V;
      var FR = FG + FRR + FD; //Fresist

      var WRK = FR * D; //Work
      var T = 3.6 * D / V; //Time

      $scope.data.timeSelection = T;
    }

  })

  .controller('ProfileCtrl', function ($scope, $rootScope, $ionicModal, $stateParams, $window, $state, homeFactory) {

    $scope.mmm = {}
    $scope.mmm.isMyProfile=true;
    $rootScope.user_profile_data = {};
    $rootScope.user_profile_data.id = $rootScope.userID;
    $rootScope.user_profile_data.avatar = $rootScope.profile_data.avatar;
    $rootScope.user_profile_data.displayname = $rootScope.profile_data.displayname;
    $rootScope.user_profile_data.ftp = $rootScope.profile_data.ftp;
    $rootScope.user_profile_data.weight = $rootScope.profile_data.weight;
    $rootScope.user_profile_data.bike_weight = $rootScope.profile_data.bikeweight;
    $rootScope.user_profile_data.pwr = $rootScope.profile_data.pwr;
    $rootScope.user_profile_data.country = $rootScope.profile_data.country;
    $rootScope.user_profile_data.user_col_level = $rootScope.profile_data.user_col_level;
    $rootScope.loadUserProfile = loadProfile;
    
    loadProfile();

    function loadProfile() {
      $rootScope.my_activities = [];
      
      //Console.log($stateParams.user_id);
      if ($stateParams.user_id == "empty" || $rootScope.user_open_id == $rootScope.userID) {
        $rootScope.myActivityTab();
        $scope.mmm.isMyProfile = true;
      } 
      else {
        $scope.mmm.isMyProfile = false;
        $rootScope.user_profile_data.id = $stateParams.user_id;
        //Console.log("WEIGHT: "+$stateParams.weight+"BIKE WEIGHT: "+$stateParams.bike_weight+"FTP: "+$stateParams.ftp);
        //($rootScope.user_profile_data.ftp != null && $rootScope.user_profile_data.weight != null) ? Math.round(parseFloat(result.ftp) / parseFloat(result.weight) * 10) / 10 : '';
        
        var link = 'https://www.deuxmille.cc/api/user.get_memberinfo/?user_id=' + $rootScope.user_profile_data.id + '&insecure=cool';
        homeFactory
          .apiCall(link, 'GET')
          .then(function (response) {
            var result = response.data;
            if(result.user_col_level == '')
              $rootScope.user_profile_data.user_col_level = 'No Cat';
            else
              $rootScope.user_profile_data.user_col_level = result.user_col_level;
            $rootScope.user_profile_data.user_col_level = result.user_col_level;
            $rootScope.user_profile_data.weight = result.weight;
            $rootScope.user_profile_data.bike_weight = result.bike_weight;
            $rootScope.user_profile_data.displayname = result.displayname;
            $rootScope.user_profile_data.avatar = result.avatar;
            $rootScope.user_profile_data.ftp = result.ftp;
            $rootScope.user_profile_data.country = result.country;
            $rootScope.user_profile_data.pwr = ($rootScope.user_profile_data.ftp != null && $rootScope.user_profile_data.weight != null) ? Math.round(parseFloat($rootScope.user_profile_data.ftp) / parseFloat($rootScope.user_profile_data.weight) * 10) / 10 : '0';
            loadUserActivity();
          });
      }

      link = "https://www.deuxmille.cc/api/user.get_strava_instagram_links/?user_id=" + $rootScope.user_profile_data.id + "&insecure=cool";
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          $scope.user_profile_strava_link = "https://www.strava.com/athletes/" + response.data.user_strava_id;
          $scope.user_profile_instagram_link = response.data.instagram;
        });
    }

    $scope.redirectToStrava = function(){
      if($scope.user_profile_strava_link != "https://www.strava.com/athletes/null") {
        $window.open($scope.user_profile_strava_link, '_blank');
      }
      else
        $window.open("https://www.strava.com/", '_blank');
    };

    $scope.redirectToInstagram = function(){
      var link = 'http://www.instagram.com/';
      if($scope.user_profile_instagram_link != null) {
        link += $scope.user_profile_instagram_link;
      }
      $window.open(link, '_blank');

    };

    function loadUserActivity() {
      $rootScope.my_activities = [];
      $rootScope.show();
      numPlace = 0;
      $scope.projected_time = "1:21:48";
      var link = 'https://www.deuxmille.cc/api/user/get_usercols/?user_id='+$rootScope.user_profile_data.id+'&user_call_id='+$rootScope.userID+'&insecure=cool';
      var stravaLink = 'https://www.strava.com/api/v3/segments/';
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response1) {
          $rootScope.hide();
          var usercols=response1.data.allusercols;
          var highest_col = "0,0";
          var done_cols = [];
          for(var i=0;i<usercols.length;i++) {
            var time_elapsed = '';
            if(usercols[i].time != 0)
              time_elapsed = new Date(parseInt(usercols[i].time)*1000);

            var date_event = '';
            if(usercols[i].date != 0)
              date_event = new Date(usercols[i].date*1000);

            var col_height = usercols[i].road_altitude;
            col_height = col_height.split(" ")[0];
            if(parseFloat(highest_col.replace(",",".")) < parseFloat(col_height.replace(",","."))){
              highest_col = col_height;
            }
            var gradientAvg = usercols[i].road_gradient;
            gradientAvg = gradientAvg.split("%")[0];
            var distaance = usercols[i].road_length;
            distaance = parseFloat(distaance.split(" ")[0]);
            var projected_time = new Date(($rootScope.recalculate (gradientAvg, $rootScope.user_profile_data.ftp, distaance*1000,
             parseFloat($rootScope.user_profile_data.weight) + parseFloat($rootScope.user_profile_data.bike_weight)).time)*1000);
            if(isNaN(projected_time))
              projected_time = 'uncalculated';
            var rate = 0;
            if(usercols[i].rate != null)
              rate = usercols[i].rate;
            $rootScope.my_activities.push({
              ColUserId: usercols[i].ColUserId,
              col_id: usercols[i].post_id,
              user_id: $stateParams.user_id,
              personal_avatar: $stateParams.avatar,
              personal_name: $stateParams.displayname,
              event_place: usercols[i].col_name,
              from_place: usercols[i].road_name,
              event_data: date_event,
              distance: usercols[i].road_length,
              event_time: time_elapsed,
              projected_time: projected_time,
              event_image: usercols[i].PostImage,
              ftp: $stateParams.ftp,
              weight: $stateParams.weight,
              bike_weight: $stateParams.bike_weight,
              event_place_id: usercols[i].segment_id,
              rate: rate,
              likes_count: usercols[i].likes_count,
              comments_count: usercols[i].comments_count,
              liked_by_user: usercols[i].isLiked_by_user
            });/* ftp: $rootScope.selected_feed_item.ftp,
            weight: $rootScope.selected_feed_item.weight,
            bike_weight: $rootScope.selected_feed_item.bike_weight,*/
            if(done_cols.indexOf(usercols[i].post_id) === -1)
              done_cols.push(usercols[i].post_id);
          }
          $rootScope.highest_col=highest_col;
          $rootScope.cur_user_number_of_cols = done_cols.length;
        });
    }

    $scope.profileMore = function(){
      $state.go('profile_more');
    }

    $scope.LikePost = function(value) {
      var toUTCDate = function(date){
        var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        return _utc;
      };//https://www.deuxmille.cc/api/user/user_liked_post/?user_id=372&usercol_id=13427185&datee=1553131&insecure=cool
      var link = 'https://www.deuxmille.cc/api/user/user_liked_post/?user_id='+$rootScope.userID
      +'&usercol_id='+value.ColUserId
      +'&datee='+(new Date()).getTime()/1000
      +'&insecure=cool';
      //Console.log(link);
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data == "like_added") {
            value.likes_count++;
            value.liked_by_user = true;
          }
          else {
            value.likes_count--;
            value.liked_by_user = false;
          }
        });
    }
    /*$rootScope.updatePwr = function () {
      var ftp = $rootScope.newData.ftp;
      var weight = $rootScope.newData.weight;
      if (ftp && ftp.length > 0 && weight && weight.length > 0) {
        $rootScope.newData.pwr = Math.round(parseFloat(ftp) / parseFloat(weight) * 10) / 10;
      } else {
        $rootScope.newData.pwr = '';
      }
    };*/
  })
  .controller('ProfileMoreCtrl', function ($scope, $rootScope, $ionicModal, $stateParams, $ionicHistory, $window, $state, homeFactory) {
    $scope.goBack = function() {
      var link = 'https://www.deuxmille.cc/api/user.get_memberinfo/?user_id=' + $rootScope.userID.toString() + '&insecure=cool';
      //Console.log(link);
      homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          var result = response.data;
          if (result.status == "ok") {
            $rootScope.profileResults = result;
            if (result != null) {
              $rootScope.profile_data = {};
              $rootScope.profile_data.avatar = result.avatar;
              $rootScope.profile_data.units = result.units != null ? result.units : '';
              $rootScope.profile_data.ftp = result.ftp != null ? result.ftp : '';
              $rootScope.profile_data.weight = result.weight != null ? result.weight : '';
              $rootScope.profile_data.bikeweight = result.bike_weight != null ? result.bike_weight : '';
              $rootScope.profile_data.pwr = (result.ftp != null && result.weight != null) ? Math.round(parseFloat(result.ftp) / parseFloat(result.weight) * 10) / 10 : '';
              $rootScope.profile_data.name = result.name;
              $rootScope.profile_data.displayname = result.displayname;
              $rootScope.profile_data.country = result.country;
              if((result.user_col_level == '') || (result.user_col_level == null))
                $rootScope.profile_data.user_col_level = 'No Cat';
              else
                $rootScope.profile_data.user_col_level = result.user_col_level;
              //$rootScope.checkProfileData($ionicModal);
            }
          } else {
            navigator.notification.alert('No profile data!', function () {
            }, 'Error');
          }
          $ionicHistory.goBack();
        });
    };
    $scope.editProfile = function() {
      $state.go('profile_settings');
    };
    $scope.notificationsSettings = function() {
      $state.go('profile_settings_notification');
    };
    $scope.termsConditions = function(){
      $window.open('https://www.deuxmille.cc/deuxmille/', '_blank');
    };
  })
  .controller('ProfileSettingsCtrl', function ($scope, $rootScope, $ionicModal, $stateParams, $ionicHistory, $window, $state, homeFactory, $cordovaCamera, $cordovaInAppBrowser) {

    var all_segments = [];
    $scope.CheckProfileInputs = {};
    /*var link = 'https://www.deuxmille.cc/api/user/get_user_profile_data/?user_id='+$rootScope.userID+'&insecure=cool';
    $scope.profile = {
      profilePhoto : $rootScope.profile_data.avatar,
      ftp : $rootScope.profile_data.ftp,
      bikeweight : $rootScope.profile_data.bikeweight,
      country : $rootScope.profile_data.country,
      first_name : "",
      last_name : "",
      billing_city : "",
      instagram : "",
      bio : ""
    };
*/
      $rootScope.show();
      var link = 'https://www.deuxmille.cc/api/user/get_user_profile_data/?user_id='+$rootScope.userID+'&insecure=cool';
      $scope.profile = {
        profilePhoto : $rootScope.profile_data.avatar,
        ftp : $rootScope.profile_data.ftp,
        bikeweight : $rootScope.profile_data.bikeweight,
        country : $rootScope.profile_data.country,
        first_name : "",
        last_name : "",
        billing_city : "",
        instagram : "",
        bio : ""
      };
      homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          var result = response.data;
          $scope.profile.first_name = result.first_name;
          $scope.profile.last_name = result.last_name;
          $scope.profile.billing_city = result.billing_city;
          $scope.profile.instagram = "http://www.instagram.com/"+result.instagram;
          $scope.profile.bio = result.description;
          $scope.profile.driver_weight = result.weight;
          $rootScope.hide();
        });

   /* homeFactory
      .apiCall(link, 'GET')
      .then(function (response) {
        var result = response.data;
        $scope.profile.first_name = result.first_name;
        $scope.profile.last_name = result.last_name;
        $scope.profile.billing_city = result.billing_city;
        $scope.profile.instagram = result.instagram;
        $scope.profile.bio = result.description;
        $scope.profile.driver_weight = result.driver_weight;
      });*/
      
  
      /*function onBackButton() {
        $rootScope.show();
        all_segments = [];
        var link = "https://www.deuxmille.cc/api/user/get_user_stravaToken/?insecure=cool&user_id="+$rootScope.userID;
        homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          link = "https://www.strava.com/api/v3/segments/starred";
          var token=response.data;
          homeFactory
            .stravaAPI(link, 'GET', token)
            .then(function (response2) {
              var userSegmentsIds = [];
              ////Console.log(JSON.stringify(response.data));
              for(var i=0;i<response2.data.length;i++)
                userSegmentsIds.push({
                  segment_id: response2.data[i].id,
                  date: new Date(response2.data[i].starred_date),
                  elapsed_time: 3306
                });
              link = "https://www.deuxmille.cc/api/user/get_all_strava_segments/?insecure=cool"
              homeFactory
                .apiCall(link, 'GET')
                .then(function (response2) {
                  all_segments = response2.data.all_segments;
                  ////Console.log(JSON.stringify(all_segments));
                  GetStravaActivity(userSegmentsIds, token);
                  $rootScope.hide();
                });
            },
            function(error) {
              alert('Something went wrong');
              $rootScope.hide();
            });
        });
        $rootScope.show();
        all_segments = [];
        var link = "https://www.deuxmille.cc/api/user/get_user_stravaToken/?insecure=cool&user_id="+$rootScope.userID;
        homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          link = "https://www.strava.com/api/v3/athlete/activities";
          var token=response.data;
          homeFactory
            .stravaAPI(link, 'GET', token)
            .then(function (response2) {
              var allActivitiesIds = [];
              ////Console.log(JSON.stringify(response.data));
              for(var i=0;i<response2.data.length;i++)
                allActivitiesIds.push(response2.data[i].id);
              link = "https://www.deuxmille.cc/api/user/get_all_strava_segments/?insecure=cool"
              homeFactory
                .apiCall(link, 'GET')
                .then(function (response2) {
                  all_segments = response2.data.all_segments;
                  ////Console.log(JSON.stringify(all_segments));
                  GetStravaActivity(allActivitiesIds, token);
                  $rootScope.hide();
                });
            });
        });
      }
  
      function GetStravaActivity(userSegmentsIds, token) {
        var segmentsIds = [];
        var segments = [];
        for(var i=0;i<userSegmentsIds.length;i++) {
          if(!isDeuxMilleSegment(userSegmentsIds[i].id))
            continue;
          link = "https://www.deuxmille.cc/api/posts/add_new_post/?u_id="+$rootScope.userID+"&s_id="+userSegmentsIds[i].id+
          "&c_id=-1&e_id=0&edate="+userSegmentsIds[i].date.getTime()/1000+"&etime="+segments_efforts[j].elapsed_time+
          "&insecure=cool";
          //Console.log(link);
          homeFactory
            .colsCall(link, 'GET')
            .then(function (response) {
            });
          }
      }*/
      

    $scope.getPhoto = function() {

      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        encodingType: Camera.EncodingType.PNG,
        allowEdit : true,
        targetWidth:130,
        targetHeight:130
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
        //Console.log("OUR PICTURE: "+imageData);
        $scope.profile.profilePhoto = "data:image/png;base64," + imageData;
      }, function(err) {
        // error
      });


    };

    $scope.logout = function () {
      //Console.log('logout click!');
      NativeStorage.remove('user_auth', function () {
        //Console.log('Successfully removed from storage');
      }, function (error) {
        //Console.log(error);

      });
      $rootScope.profile_data = {};
      $state.go('login');
    };
    $scope.updateProfile = function () {
      $rootScope.show();
      var link = "https://www.deuxmille.cc/api/user/change_profile_photo/?insecure=cool&user_id="+$rootScope.userID;
      //var g = $scope.profile.profilePhoto;
      if($scope.profile.profilePhoto != $rootScope.profile_data.avatar) {
        var dataa = "image="+$scope.profile.profilePhoto;
        //var dataa = { ID:14, image:'someString' };
        homeFactory
          .postImageCall(link, 'POST', dataa)
          .then(function (response) {
            //$rootScope.profile_data.avatar = $scope.profile.profilePhoto;
          });//wp-content/uploads/Col_images/image.png
      }
      $scope.CheckProfileInputs.first_name = false;
      $scope.CheckProfileInputs.last_name = false;
      $scope.CheckProfileInputs.billing_city = false;
      $scope.CheckProfileInputs.ftp = false;
      $scope.CheckProfileInputs.bikeweight = false;
      $scope.CheckProfileInputs.driver_weight = false;

      if($scope.profile.first_name == ""){
        $scope.CheckProfileInputs.first_name = true;
        $rootScope.hide();
        return;
      }
      if($scope.profile.last_name == ""){
        $scope.CheckProfileInputs.last_name = true;
        $rootScope.hide();
        return;
      }
      if($scope.profile.ftp == "" || isNaN($scope.profile.ftp) ){
        $scope.CheckProfileInputs.ftp = true;
        $rootScope.hide();
        return;
      }
      
      if($scope.profile.bikeweight == "" || isNaN($scope.profile.bikeweight) ){
        $scope.CheckProfileInputs.bikeweight = true;
        $rootScope.hide();
        return;
      }
      
      if($scope.profile.driver_weight == "" || isNaN($scope.profile.driver_weight) ){
        $scope.CheckProfileInputs.driver_weight = true;
        $rootScope.hide();
        return;
      }
      if($scope.profile.country == null)
        $scope.profile.country = "";
      if($scope.profile.city == null)
        $scope.profile.city = "";
      link = 'https://www.deuxmille.cc/api/user/update_user_profile_data/' +
        '?user_id='+$rootScope.userID +
        '&first_name='+$scope.profile.first_name +
        '&last_name='+$scope.profile.last_name +
        '&city='+$scope.profile.billing_city +
        '&instagram='+$scope.profile.instagram.replace("http://www.instagram.com/","") +
        '&description='+$scope.profile.bio +
        '&ftp='+$scope.profile.ftp +
        '&weight='+$scope.profile.driver_weight +
        '&bike_weight='+$scope.profile.bikeweight +
        '&country='+$scope.profile.country +
        '&display_name='+$scope.profile.first_name + " " + $scope.profile.last_name +
        '&driver_weight='+$scope.profile.driver_weight +
        '&user_login='+$scope.profile.first_name.toLowerCase() + "." + $scope.profile.last_name.toLowerCase() +
        '&user_nicename='+$scope.profile.first_name.toLowerCase() + "-" + $scope.profile.last_name.toLowerCase() +

        '&um_user_profile_url_slug_name='+$scope.profile.first_name + "." + $scope.profile.last_name +
        '&nickname='+$scope.profile.first_name.toLowerCase() + "." + $scope.profile.last_name.toLowerCase() +
        '&billing_first_name='+$scope.profile.first_name +
        '&billing_last_name='+$scope.profile.last_name +
        '&full_name='+$scope.profile.first_name.toLowerCase() + " " + $scope.profile.last_name.toLowerCase() +
        '&insecure=cool';

      //https://www.deuxmille.cc/api/user/update_user_profile_data/?user_id=312&first_name=VVVVVVV&last_name=KKKKKKKKK&city=KKKKKKKK&instagram=Intagram&description=description&ftp=22222&weight=80&bike_weight=8&country=Uuuuuu&display_name=VVVVV KKKKKK&driver_weight=72&insecure=cool

      $rootScope.profile_data.avatar = $scope.profile.profilePhoto;
      $rootScope.profile_data.ftp = $scope.profile.ftp;
      $rootScope.profile_data.weight = $scope.profile.weight;
      $rootScope.profile_data.bikeweight = $scope.profile.bikeweight;
      $rootScope.profile_data.country = $scope.profile.country;
      $rootScope.profile_data.displayname = $scope.profile.first_name + " " + $scope.profile.last_name;
      $rootScope.profile_data.pwr = ($scope.profile.ftp != null && $scope.profile.weight != null) ? Math.round(parseFloat($scope.profile.ftp) / parseFloat($scope.profile.weight) * 10) / 10 : '';
      homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
            $rootScope.hide();
            $state.go('profile_more');
          //$rootScope.myActivityTab();
        });
    };

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

  })
  .controller('ProfileSettingsNotificationCtrl', function ($scope, $rootScope, $ionicModal, $stateParams, $ionicHistory, $window, $state, homeFactory) {
    $rootScope.checks = {
      receive_newsletters:false,
      notificate_likes:false,
      notificate_comments:false
    };
      $rootScope.show();
      $rootScope.checks = {
        receive_newsletters:false,
        notificate_likes:false,
        notificate_comments:false
      };
        var link = 'https://www.deuxmille.cc/api/user.get_user_settings/?user_id='+$rootScope.userID+'&insecure=cool';
        homeFactory
          .apiCall(link, 'GET')
          .then(function (response) {
            var result = response.data;
            $rootScope.checks.receive_newsletters = (result.receive_newsletters === "true");
            $rootScope.checks.notificate_likes = (result.notificate_likes === "true");
            $rootScope.checks.notificate_comments = (result.notificate_comments === "true");
            $rootScope.hide();
          });

    $scope.saveSettings= function(){
      link = "https://www.deuxmille.cc/api/user.save_user_settings/?user_id="+ $rootScope.userID
        +"&letters="+ $rootScope.checks.receive_newsletters
        +"&likes="+ $rootScope.checks.notificate_likes
        +"&comments="+ $rootScope.checks.notificate_comments
        +"&insecure=cool";
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          $ionicHistory.goBack();
        });
    };

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };
  })

  .controller('UserColsCtrl', function ($scope, $rootScope, homeFactory) {


    $scope.showColDetails = function (col) {
      navigator.notification.alert('Time: ' + col.col_time + '\nDate: ' + col.col_date, function() {}, 'Information');
    }
  })

  .controller('FeedCtrl', function ($scope, $state, $rootScope, $cordovaInAppBrowser, $interval, homeFactory) {
    loadMembersActivity();
    $rootScope.user_open_id = $rootScope.userID;
    $rootScope.item_array = [];
    $rootScope.myActivityTab = loadMyActivity;
    $scope.membersActivityTab = loadMembersActivity;
    $scope.TimerForNottsF = null;
    $scope.NewNotifications = false;
    $rootScope.IsNotificationPage = false;
    CheckNewNots({name: 'tab.feed'});    

    $rootScope.$on('$stateChangeStart', function(evt, toState, toParams, fromState, fromParams) {
      CheckNewNots(toState);
    });

    function CheckNewNots(toState) {
      var LinkForNewNotifs = 'https://www.deuxmille.cc/api/user/get_unreaded_notifications/?insecure=coo&user_id='+$rootScope.userID;
      if(toState.name == 'tab.feed') {
        $scope.TimerForNottsF = $interval(function () {
          homeFactory
          .colsCall(LinkForNewNotifs, 'GET')
          .then(function (response) {
            if(response.data == true)
              $scope.NewNotifications = true;
            else
              $scope.NewNotifications = false;
          });
        }, 1500);
      }
      else {
          $interval.cancel($scope.TimerForNottsF);
      }
    }

    function loadMyActivity() {
      $scope.isMyActivity = true;
      $rootScope.item_array = [];
      //$rootScope.my_activities = [];
      //Console.log("Feed->loadMyActivity");
      //$scope.event_image = "img/Rectangle3.png";
      $rootScope.show();
      numPlace = 0;
      $scope.projected_time = "1:21:48";
      var link = 'https://www.deuxmille.cc/api/user/get_usercols/?user_id='+$rootScope.userID+'&user_call_id='+$rootScope.userID+'&insecure=cool';
      var stravaLink = 'https://www.strava.com/api/v3/segments/';
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response1) {
          $rootScope.hide();
          var usercols=response1.data.allusercols;
          var highest_col = "0,0";
          var done_cols = [];
          for(var i=0;i<usercols.length;i++) {

            if(usercols[i].time != 0)
              var time_elapsed = new Date(parseInt(usercols[i].time)*1000);
            
            var date_event = '';
            if(usercols[i].date != 0)
              date_event = new Date(usercols[i].date*1000);

            var col_height = usercols[i].road_altitude;
            col_height = col_height.split(" ")[0];
            if(parseFloat(highest_col.replace(",",".")) < parseFloat(col_height.replace(",","."))){
              highest_col = col_height;
            }
            var gradientAvg = usercols[i].road_gradient;
            gradientAvg = gradientAvg.split("%")[0];
            var distaance = usercols[i].road_length;
            distaance = parseFloat(distaance.split(" ")[0]);
            var projected_time = new Date(($rootScope.recalculate (gradientAvg, $rootScope.profile_data.ftp, distaance*1000,
             parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight)).time)*1000);
            if(isNaN(projected_time))
              projected_time = 'uncalculated';

            var rate = 0;
            if(usercols[i].rate != null)
              rate = usercols[i].rate;
            $rootScope.item_array.push({
              ColUserId: usercols[i].ColUserId,
              col_id: usercols[i].post_id,
              user_id: $rootScope.userID,
              personal_avatar: $rootScope.profile_data.avatar,
              personal_name: $rootScope.profile_data.displayname,
              event_place: usercols[i].col_name,
              from_place: usercols[i].road_name,
              event_data: date_event,
              distance: usercols[i].road_length,
              event_time: time_elapsed,
              projected_time: projected_time,
              event_image: usercols[i].PostImage,
              //ftp: $rootScope.profile_data.ftp,
              //weight: $rootScope.profile_data.weight,
              //bike_weight: $rootScope.profile_data.bikeweight,
              event_place_id: usercols[i].segment_id,
              rate: rate,
              likes_count: usercols[i].likes_count,
              comments_count: usercols[i].comments_count,
              liked_by_user: usercols[i].isLiked_by_user
            });
            $rootScope.my_activities.push($rootScope.item_array[$rootScope.item_array.length - 1]);
            if(done_cols.indexOf(usercols[i].post_id) === -1)
              done_cols.push(usercols[i].post_id);
          }
          $rootScope.highest_col=highest_col;
          $rootScope.cur_user_number_of_cols = done_cols.length;
        });
    }

    function loadMembersActivity() {
      $scope.isMyActivity = false;
      $rootScope.show();
      $rootScope.item_array = [];
      //$scope.event_image = "img/Rectangle3.png";
      //while($rootScope.userID == null)
      ////Console.log(JSON.stringify($rootScope.userID));
      var link = 'https://www.deuxmille.cc/api/user/get_feed/?user_id='+$rootScope.userID+'&insecure=cool';
      //Console.log(link);
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          $rootScope.hide();
          var feed_items=response.data.feed_items;
          for(var i=0;i<feed_items.length;i++) {
            var time_elapsed = '';
            if(feed_items[i].time != 0)
              time_elapsed = new Date(feed_items[i].time*1000);
              
            var date_event = '';
            if(feed_items[i].date != 0)
              date_event = new Date(feed_items[i].date*1000);

            var gradientAvg = feed_items[i].road_gradient;
            gradientAvg = gradientAvg.split("%")[0];
            var distaance = feed_items[i].road_length;
            distaance = parseFloat(distaance.split(" ")[0]);
            var projected_time = new Date(($rootScope.recalculate (gradientAvg, $rootScope.profile_data.ftp, distaance*1000,
              parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight)).time)*1000);
            if(isNaN(projected_time))
              projected_time = 'uncalculated';
 


            if(feed_items[i].rate == null)
              feed_items[i].rate = 0;
            $rootScope.item_array.push({
              ColUserId: feed_items[i].ColUserId,
              col_id: feed_items[i].id,
              user_id: feed_items[i].user_id,
              personal_avatar: feed_items[i].avatar,
              personal_name: feed_items[i].display_name,
              event_place: feed_items[i].col_name,
              from_place: feed_items[i].road_name,
              event_data: date_event,
              distance: feed_items[i].road_length,
              event_time: time_elapsed,
              projected_time: projected_time,
              event_image: feed_items[i].PostImage,
              //ftp: feed_items[i].ftp,
              //weight: feed_items[i].weight,
              //bike_weight: feed_items[i].bike_weight,
              event_place_id: feed_items[i].segment_id,
              rate: feed_items[i].rate,
              likes_count: feed_items[i].likes_count,
              comments_count: feed_items[i].comments_count,
              liked_by_user: feed_items[i].isLiked_by_user,

            });

          }
        });
    }
    $rootScope.feed_item_click = function (value) {
      $rootScope.selected_feed_item = value;
      $state.go('feed_item');
    };
    $scope.notifications_click = function (value) {
      $state.go('notifications');
    }
    $scope.LikePost = function(value) {
      var toUTCDate = function(date){
        var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        return _utc;
      };//https://www.deuxmille.cc/api/user/user_liked_post/?user_id=372&usercol_id=13427185&datee=1553131&insecure=cool
      var link = 'https://www.deuxmille.cc/api/user/user_liked_post/?user_id='+$rootScope.userID
      +'&usercol_id='+value.ColUserId
      +'&datee='+(new Date()).getTime()/1000
      +'&insecure=cool';
      //Console.log(link);
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data == "like_added") {
            value.likes_count++;
            value.liked_by_user = true;
          }
          else {
            value.likes_count--;
            value.liked_by_user = false;
          }
        });
    }
    $rootScope.ConnectToStravaNEW = function(isSwitch) {
      var link = "https://www.strava.com/oauth/authorize?client_id=24975&response_type=code";//24975
      var options = 'location=yes';
      if(isSwitch) {
        options = ',clearcache=yes'
      }
      else {
        options = ',clearcache=no'
      }
      //link += "&redirect_uri=https%3A%2F%2Fwww.deuxmille.cc%2Fwp-content%2Fplugins%2Fjson-api-user%2Fcontrollers%2FSuccess_strava_synch.php%2F%3Fuser_id%3D" + $rootScope.userID + "%26insecure%3Dcool";
      link += "&redirect_uri=http%3A%2F%2Flocalhost%2Fcallback";
      var ref = window.open(link, '_blank', options);//$cordovaInAppBrowser.open(link, '_blank', options);
      ref.addEventListener('loadstart', function(event) {
        if((event.url.toString()).indexOf("http://localhost/callback",0) > -1) {
            requestToken = (event.url).split("code=")[1];
            ref.executeScript(
              {code: "console.log('check');"},
              function(data)
              {
                ref.close();
              }
            );
            ref.executeScript(
              {code: "console.log('check');"},
              function(data)
              {
                ref.close();
                updateMainStrava(requestToken);
              });
        }
      });
      //ref.addEventListener("exit", function(event){  onBackButton(); });
    }

    function updateMainStrava(mobileStrava) {
      link = "https://www.strava.com/oauth/authorize?client_id=15599&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fcallback";
      var ref = window.open(link, '_blank', 'location=yes,clearcache=no');//$cordovaInAppBrowser.open(link, '_blank', options);
      ref.addEventListener('loadstart', function(event) {
        if((event.url.toString()).indexOf("http://localhost/callback",0) > -1) {
            requestToken = (event.url).split("code=")[1];
            ref.executeScript(
              {code: "console.log('check');"},
              function(data)
              {
                ref.close();
              }
            );
            ref.executeScript(
              {code: "console.log('check');"},
              function(data)
              {
                ref.close();
                $rootScope.show();
                link = "https://www.deuxmille.cc/api/user/set_user_strava_settings/?insecure=cool&MainStrava=trues&user_id="+$rootScope.userID+"&code="+requestToken;
                homeFactory
                  .colsCall(link, 'GET')
                  .then(function (response) {
                    updateUserStravaToken(mobileStrava);
                  });
              });
        }
      });
    }

    function updateUserStravaToken(code) {
        var form = "";
        var link = "https://www.strava.com/oauth/token";
        form += "client_id=24975";
        form += "&client_secret=d8e8893508393842c91cc5f469671f28d306ab56";
        form += "&code="+code;
        homeFactory
        .updateStravaTokens(link, 'POST', form)
        .then(function (response) {
          var tokenn = response.data.access_token;
          link = "https://www.deuxmille.cc/api/user/set_user_strava_settings/?&MainStrava=falses&user_id="+$rootScope.userID+"&token=" + tokenn + "&code=" + code + "&insecure=cool";
          homeFactory
            .colsCall(link, 'GET')
            .then(function (response1) {
              onBackButton();
            });
        },
        function (response) {
          alert('Something went wrong code 2');
          $rootScope.hide();
        }
      );
    }

    function onBackButton() {
      all_segments = [];
      var link = "https://www.deuxmille.cc/api/user/get_user_stravaToken/?insecure=cool&user_id="+$rootScope.userID;
      homeFactory
      .apiCall(link, 'GET')
      .then(function (response) {
        link = "https://www.strava.com/api/v3/athlete/activities";
        var token=response.data;/*"2a96633b09d6e42926c5f17218af95215e69cabe";*/
        //console.log(JSON.stringify(response));
        if(token == null) {
          alert('Something went wrong code 1');
          $rootScope.hide();
          return;
        }
        homeFactory
          .stravaAPI(link, 'GET', token)
          .then(function (response2) {
            var allActivitiesIds = [];
            ////Console.log(JSON.stringify(response.data));
            for(var i=0;i<response2.data.length;i++)
              allActivitiesIds.push(response2.data[i].id);
            link = "https://www.deuxmille.cc/api/user/get_all_strava_segments/?insecure=cool"
            homeFactory
              .apiCall(link, 'GET')
              .then(function (response2) {
                all_segments = response2.data.all_segments;
                ////Console.log(JSON.stringify(all_segments));
                GetStravaActivity(allActivitiesIds, token);
                updateProfile_category();
                $rootScope.hide();
                navigator.notification.alert('Your account has been synchronized', function () {}, 'Perfect!');
              });
          },
          function(error) {
              alert('Something went wrong code 3');
              $rootScope.hide();
            });
      });
    }

    function updateProfile_category() {
      var link = 'https://www.deuxmille.cc/api/user.get_memberinfo/?user_id=' + $rootScope.userID.toString() + '&insecure=cool';
      //Console.log(link);
      homeFactory
        .apiCall(link, 'GET')
        .then(function (response) {
          var result = response.data;
          if (result.status == "ok") {
            $rootScope.profileResults = result;
            if (result != null) {
              if(result.user_col_level == '')
                $rootScope.profile_data.user_col_level = 'No Cat';
              else
                $rootScope.profile_data.user_col_level = result.user_col_level;
              //$rootScope.checkProfileData($ionicModal);
            }
          } else {
            navigator.notification.alert('No profile data!', function () {
            }, 'Error');
          }
        });
    }

    /**/

    function GetStravaActivity(allActivitiesIds, token) {
      var segmentsIds = [];
      var segments = [];
      var link = "https://www.strava.com/api/v3/activities/";
      for(var i=0;i<allActivitiesIds.length;i++) {
        var link2 = link+allActivitiesIds[i]+'?include_all_efforts=';
        //Console.log(link2);
        homeFactory
          .stravaAPI(link2, 'GET', token)
          .then(function (response) {
            var segments_efforts = response.data.segment_efforts;
            for(var j=0;j<segments_efforts.length;j++) {
              if(!isDeuxMilleSegment(segments_efforts[j].segment.id))
                continue;
              var date = new Date(segments_efforts[j].start_date);
              link = "https://www.deuxmille.cc/api/posts/add_new_post/?u_id="+$rootScope.userID+"&s_id="+segments_efforts[j].segment.id+
                "&c_id=-1&e_id=0&edate="+date.getTime()/1000+"&etime="+segments_efforts[j].elapsed_time+
                "&insecure=cool";
              //Console.log(link);
              homeFactory
                .colsCall(link, 'GET')
                .then(function (response) {
                });
            }
        });
      }
      //
            
    }

    function isDeuxMilleSegment(segment_id) {
      for(var i=0;i<all_segments.length;i++) {
        if(segment_id == all_segments[i].meta_value) {
          //Console.log(segment_id);
          return true;
        }
      }
      return false;
    }
  
  })

  .controller('FeedItemCtrl', function ($scope, $rootScope, $state, $ionicHistory, $interval, homeFactory) {
    $scope.visible = false;
    $scope.commentaries = { comment: ''};
    $scope.feedItemComments = [];
    $scope.TimerForComments = null;
    $scope.PathToLikeImg = 'img/like.png';
    UpdateFeedItem();

    function UpdateFeedItem() {
      $scope.feedItemComments = [];
      if($rootScope.selected_feed_item.liked_by_user) {
        $scope.PathToLikeImg = 'img/like_click.png';
      }
      getComments();
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

    $scope.Edit = function() {
      $scope.visible = !$scope.visible;
    }

    $scope.changeCoverPhoto = function() {
      $scope.visible = !$scope.visible;
      $rootScope.isNewPost_Add.ColId = $rootScope.selected_feed_item.col_id;
      $rootScope.isNewPost_Add.Add = false;
      $state.go('album', {isNewPost: false});
    }

    $scope.DeleteThisPost = function() {
      var link = "https://www.deuxmille.cc/api/posts/remove_userCol/?ColUserId=" + $rootScope.selected_feed_item.ColUserId+"&user_id="+$rootScope.userID;
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data.status == 'success'){
            for(var i=0;i<$rootScope.item_array.length;i++) {
              if($rootScope.selected_feed_item.ColUserId == $rootScope.item_array[i].ColUserId)
                $rootScope.item_array.splice(i,1);

            }
            if($rootScope.cols_array != null)
              for(var i=0;i<$rootScope.cols_array.length;i++) {
                if($rootScope.cols_array[i].col_id == $rootScope.selected_feed_item.col_id){
                  $rootScope.cols_array[i].isChecked = false;
                  break;
                }
              }
          }
          $scope.visible = !$scope.visible;
          $ionicHistory.goBack();
        });
    }

    $scope.compareToOtherMembers = function() {
      $state.go('col_detail', {col_id: $rootScope.selected_feed_item.col_id});
    };

    $scope.openProfile = function() {
      $rootScope.user_open_id = $rootScope.selected_feed_item.user_id;
      $state.go('tab.profile', {user_id: $rootScope.selected_feed_item.user_id,
        ftp: $rootScope.selected_feed_item.ftp,
        weight: $rootScope.selected_feed_item.weight,
        bike_weight: $rootScope.selected_feed_item.bike_weight,
        avatar: $rootScope.selected_feed_item.personal_avatar,
        displayname: $rootScope.selected_feed_item.personal_name});
    };

    $scope.WriteComment = function(keyEvent) {
      if (keyEvent.which === 13) {
        keyEvent.target.blur();
        LeaveComment();//InputTime
      }
    }

    function LeaveComment() {
      $rootScope.show();
      var commentt = $scope.commentaries.comment;
      if(commentt == ""){
        alert("Empty comment");
        $rootScope.hide();
        return;
      }
      commentt = encodeURI(commentt);
      var link = 'https://www.deuxmille.cc/api/user/user_commented_post/?user_id='+$rootScope.userID
      +'&usercol_id='+$rootScope.selected_feed_item.ColUserId
      +'&commentary='+commentt
      +'&datee='+(new Date()).getTime()/1000
      +'&insecure=cool';
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var commentar = $scope.commentaries.comment.slice(0);
          var daate = new Date();
          $rootScope.hide();
          if(response.data == "ok") {
            $scope.feedItemComments.push({
              display_name:$rootScope.profile_data.displayname,
              commentary:commentar,
              date:daate,
              avatar:$rootScope.profile_data.avatar
            });
          }
          $scope.commentaries.comment = '';
          $rootScope.selected_feed_item.comments_count++;
        });
      //Console.log(link);
    }

    $scope.LikePost = function() {
      var toUTCDate = function(date){
        var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        return _utc;
      };//https://www.deuxmille.cc/api/user/user_liked_post/?user_id=372&usercol_id=13427185&datee=1553131&insecure=cool
      var link = 'https://www.deuxmille.cc/api/user/user_liked_post/?user_id='+$rootScope.userID
      +'&usercol_id='+$rootScope.selected_feed_item.ColUserId
      +'&datee='+(new Date()).getTime()/1000
      +'&insecure=cool';
      //Console.log(link);
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data == "like_added") {
            $scope.PathToLikeImg = 'img/like_click.png';
            $rootScope.selected_feed_item.likes_count++;
            $rootScope.selected_feed_item.liked_by_user = true;
          }
          else {
            $scope.PathToLikeImg = 'img/like.png';
            $rootScope.selected_feed_item.likes_count--;
            $rootScope.selected_feed_item.liked_by_user = false;
          }
        });
    }

    $scope.checkForAuthorizy = function(id) {
      if(id==$rootScope.userID)
        return true;
      return false;
    }

    function getComments() {
      var link = 'https://www.deuxmille.cc/api/user/get_comments_post/?insecure=cool&user_id=' + $rootScope.selected_feed_item.user_id + '&usercol_id=' + $rootScope.selected_feed_item.ColUserId;
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var all_comments = response.data.comments;
          /*
                  <h5 class="personal_name">{{item.display_name}}<span>{{item.commentary}}</span></h5>
                  <p class="event_data">{{item.date | date:'MMMM d'}} at {{item.date | date:'h:mm a'}} </p>
                */
          for(var i=0;i<all_comments.length;i++) {
            var daate = new Date(all_comments[i].Date*1000);
            daate = new Date(daate);
            $scope.feedItemComments.push({
              display_name:all_comments[i].display_name,
              commentary:all_comments[i].commentary,
              date:daate,
              avatar:all_comments[i].avatar
            });
          }
        });
    }
  })

  .controller('AlbumCtrl', function ($scope, $stateParams, $state, $ionicHistory, $rootScope, homeFactory) {
    //InitThis();

      InitThis();

    $scope.Cancel = function() {
      $ionicHistory.goBack();
    }
    function InitThis() {
      $scope.album_images = [];
      var link = 'https://www.deuxmille.cc/api/posts/get_images_of_col/?c_id='+$rootScope.isNewPost_Add.ColId;
      homeFactory
          .colsCall(link, 'GET')
          .then(function (response) {
            var images = response.data.images;
            for(var i=0;i<images.length;i++) {
              $scope.album_images.push({url: images[i].guid, isChoosed: false});
            }
            $scope.album_images[0].isChoosed = true;
          });
    }
    $scope.Done = function() {
      //in this we should send http request for change item.
      //Then go back to feed-item page.
      if($rootScope.isNewPost_Add.Add) {
        for(var i=0;i<$scope.album_images.length;i++)
          if($scope.album_images[i].isChoosed) {
            var immg = encodeURI($scope.album_images[i].url);
            $rootScope.LinkToImgNewPostNewPost = immg;
            //$rootScope.LinkToImgNewPostNewPost = "https://www.deuxmille.cc//wp-content//uploads//Col_images//rectangle4.png"
            break;
          }
      }
      else {
        for(var i=0;i<$scope.album_images.length;i++)
          if($scope.album_images[i].isChoosed) {
            var immg = encodeURI($scope.album_images[i].url);
            var link = 'https://www.deuxmille.cc/api/user/set_user_col_image/?insecure=cool&user_id='+$rootScope.userID
            +'&colUserId='+$rootScope.selected_feed_item.ColUserId+'&image='+immg;
            $rootScope.selected_feed_item.event_image = $scope.album_images[i].url;
            for(var j=0;j<$rootScope.item_array.length;j++) {
              if($rootScope.selected_feed_item.ColUserId == $rootScope.item_array[j].ColUserId) {
                $rootScope.item_array[j].event_image = $scope.album_images[i].url;
                break;
              }
            }
            homeFactory
            .colsCall(link, 'GET')
            .then(function (response) {
            });
            break;
          }
      }
      $ionicHistory.goBack();
    }

    $scope.Choose = function(item) {
      for(var i=0;i<$scope.album_images.length;i++) {
        $scope.album_images[i].isChoosed = false;
      }
      item.isChoosed = true;
    }
  })

  .controller('NotificationsCtrl', function ($scope, $state, $ionicHistory, $rootScope, $interval, $compile, homeFactory) {
    $scope.notificationItems = [];
    $scope.LoadNotifications = loadNotifications;
    $scope.TimerForNotts = null;
    var link = "";
    var linkUpdate = "";
    StartTimerForNots();
    $rootScope.IsNotificationPage = true;
    
    function StartTimerForNots() {
      $scope.notificationItems = [];
      link = 'https://www.deuxmille.cc/api/user/get_user_notifications/?insecure=cool&user_id='+$rootScope.userID;
      linkSetNotsReaded = 'https://www.deuxmille.cc/api/user/read_user_notifications/?insecure=cool&user_id='+$rootScope.userID;
      loadNotifications();
      //}, 1000);
    }

    function loadNotifications() {
      linkUpdate = 'https://www.deuxmille.cc/api/user/update_user_notifications/?insecure=cool&user_id='+$rootScope.userID;
      homeFactory
      .colsCall(link, 'GET')
      .then(function (response) {
        for(var i=0;i<response.data.notifications.length;i++) {
          var notification = response.data.notifications[i];
          PushToArray(notification);
          homeFactory.colsCall(linkSetNotsReaded, 'GET').then(function (response) {});
        }
        $scope.TimerForNotts = $interval(function () {
          homeFactory
          .colsCall(linkUpdate, 'GET')
          .then(function (response) {
            if(response.data.notifications != null)
              for(var i=0;i<response.data.notifications.length;i++) {
                var notification = response.data.notifications[i];
                PushToArray(notification);
              }
              homeFactory.colsCall(linkSetNotsReaded, 'GET').then(function (response) {});
          });
        }, 5000);
      });
    }

    $scope.GoBack = function() {
        $interval.cancel($scope.TimerForNotts);
      $ionicHistory.goBack();
    }

    function PushToArray(notification) {
      var textt = "";
      var isYellow = false;
      if(notification.notification_type == "like") {
        var possibleName = notification.col_name.substring(0, notification.col_name.indexOf(",") - 3);
          if(possibleName != "")
            notification.col_name = possibleName;
          textt = "<span><b ng-click='GoToMember("+notification.user_id+")'>" + notification.display_name + "</b>" + " liked your ride on <b ng-click='GoToActivity("+notification.usercol_id+")'>" + notification.col_name + "</b> segment</span>";
      }
      if(notification.notification_type == "comment") {
        var possibleName = notification.col_name.substring(0, notification.col_name.indexOf(",") - 3);
          if(possibleName != "")
            notification.col_name = possibleName;
        textt = "<span><b ng-click='GoToMember("+notification.user_id+")'>" + notification.display_name + "</b>" + " commented your ride on <b ng-click='GoToActivity("+notification.usercol_id+")'>" + notification.col_name + "</b> segment</span>";
      }
      if(notification.notification_type == 'congratulation') {
        isYellow = true;
        notification.avatar = 'img/black_star_small.png';
        textt = "<span><b>Congratulations!</b><br>You have reached <b>" + notification.commentary + "</b> category</span>";
      }
      var g = "";
      //console.log(JSON.stringify($compile(textt)($scope)));
      $scope.notificationItems.unshift({
        personal_avatar: notification.avatar,
        text: ($compile(textt)($scope))[0].outerHTML,
        isYellowMarked: isYellow
      });
    }

    //$scope.GoToActivity = function(id) {
    $scope.GoToActivity = function(id) {
      
      var linkOfActivity = 'https://www.deuxmille.cc/api/user/get_activity/?insecure=cool&user_id='+$rootScope.userID+'&usercol_id='+id;
      homeFactory
          .colsCall(linkOfActivity, 'GET')
          .then(function (response) {
            console.log(JSON.stringify(response));
            var feed_item = response.data;
            var time_elapsed = new Date(feed_item.time*1000);
            var date_event = new Date(feed_item.date*1000);
            var gradientAvg = feed_item.road_gradient;
            gradientAvg = gradientAvg.split("%")[0];
            var distaance = feed_item.road_length;
            distaance = parseFloat(distaance.split(" ")[0]);
            var projected_time = new Date(($rootScope.recalculate (gradientAvg, $rootScope.profile_data.ftp, distaance*1000,
              parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight)).time)*1000);
            if(isNaN(projected_time))
              projected_time = 'uncalculated';

            $rootScope.selected_feed_item = {
              ColUserId: feed_item.id,
              col_id: feed_item.col_id,
              user_id: feed_item.user_id,
              personal_avatar: feed_item.avatar,
              personal_name: feed_item.display_name,
              event_place: feed_item.col_name,
              from_place: feed_item.road_name,
              event_data: date_event,
              distance: feed_item.road_length,
              event_time: time_elapsed,
              projected_time: projected_time,
              event_image: feed_item.PostImage,
              ftp: feed_item.ftp,
              weight: feed_item.weight,
              bike_weight: feed_item.bike_weight,
              event_place_id: feed_item.segment_id,
              rate: feed_item.rate,
              likes_count: feed_item.likes_count,
              comments_count: feed_item.comments_count,
              liked_by_user: feed_item.isLiked_by_user
            };
            $state.go('feed_item');
        });
    }

    //$scope.GoToMember = function (id) {
    $scope.GoToMember = function(id) {
      $rootScope.user_open_id = id;
      $state.go('tab.profile', {user_id: id});
    };
  })

  .controller('ColsCtrl', function ($scope, $state, $ionicHistory, $rootScope, homeFactory) {

    $rootScope.user_open_id = $rootScope.userID;
    $rootScope.InitColsCtrl = function() {
      $rootScope.show();
      var link = 'https://www.deuxmille.cc/api/posts/get_all_colls/?user_id='+$rootScope.userID;
      var link1 = 'https://www.deuxmille.cc/api/user/get_usercolsid/?user_id='+$rootScope.userID+'&insecure=cool';
      var linkMostPopular = 'https://www.deuxmille.cc/api/posts/get_most_popular_col/?insecure=cool';
      homeFactory
      .colsCall(link1, 'GET')
      .then(function (response1) {
        var usercolsid = response1.data.allusercolsid;
        $rootScope.cols_array = [];
        homeFactory
          .colsCall(link, 'GET')
          .then(function (response) {
            var col = response.data.most_popular;
            var col_id = col.col_id;
            var name = col.title;
            var height = col.title;
            var col_image = col.image;
            var isChecked = false;
            name = name.split('(')[0];
            name = name.substring(0, name.length - 1);
            height = height.split('(')[1];
            height = height.substring(0, height.length - 1);

            for(var j=0;j<usercolsid.length;j++){
              if(usercolsid[j].col_id==col_id){
                isChecked = true;
                break;
              }
            }

            $scope.MostPopularCol = {
              col_id: col_id,
              name_col: name,
              height_col: height + 'm',
              image_col:col_image,
              isChecked: isChecked };
            var posts = response.data.posts;
            for(var i=0;i<posts.length;i++) {
              var post = posts[i];
              var col_id = post.col_id;
              var name = post.title;
              var image = post.image;
              var possibleName = post.title.substring(0, post.title.indexOf(",") - 3);
              if(possibleName != "")
                name = possibleName;

              var height = post.height;
              var isVisit = post.visited;
              //name = name.split('(')[0];
              //height = height.split('(')[1];
              if(name != null && height != null) {
                //height = height.substring(0, height.length - 1);
                var height_num = parseFloat(height.split(' ')[0].replace(",", "."));
                if(height_num != null)
                  $rootScope.cols_array.push({
                    image_col:image,
                    col_id: col_id,
                    name_col: name,
                    height_col: height+'m',
                    height_n: height_num,
                    isChecked: isVisit
                  });
              }
            }
            $rootScope.hide();
          });
      });
    }

    $rootScope.InitColsCtrl();
    $scope.ClickCol = function(item) {
      //item.isChecked = !item.isChecked;
      $state.go('col_detail', {col_id: item.col_id, isChecked: item.isChecked});

    }

    $scope.GoToColsMap = function() {
      $state.go('all_cols_page');
    }
  })

  .controller('ColDetailCtrl', function ($scope, $state, $stateParams, $filter, $compile, $ionicHistory, $rootScope, $window, homeFactory) {
    
    $scope.Compare_member = {};
    $scope.Compare_member.display_name = '<br>Compare to other <br>members';
    $scope.Compare_member.avatar = "img/compare_2.png";
    $scope.ETime = '';
    $scope.Compare_member.CheckForComparing = false;
    $rootScope.LinkToImgNewPostNewPost = null;
    initTabs();

    $scope.Nuumdiv = $rootScope.NumDiv;
    function initTabs() {
      $scope.Compare_member.display_name = '<br>Compare to other <br>members';
      $scope.Compare_member.avatar = "img/compare_2.png"
      $scope.Compare_member.ETime = '';
      $scope.Compare_member.CheckForComparing = false;
      $rootScope.show();
      $scope.segment_rating = 0;
      $scope.all_members = [];
      $scope.members_to_compare = [];
      $scope.isCompareMembers = false;
      $scope.ActiveTab = true;
      var link = "https://www.deuxmille.cc/api/posts/get_col_details/?col_id="+$stateParams.col_id+"&user_id="+$rootScope.userID+"&insecure=cool";
      homeFactory
      .colsCall(link, 'GET')
      .then(function (response) {
        $rootScope.hide();
        $rootScope.col_tab_array = [];
        $scope.current_col_name = response.data.col_name;
        $scope.current_col_image = response.data.image;
        var possibleName = response.data.col_name.substring(0, response.data.col_name.indexOf(",") - 3);
        if(possibleName != "")
          $scope.current_col_name = possibleName;
                
        $rootScope.current_col_is_checked = response.data.isVisited;
        $scope.height = response.data.roads[0].altitude;
        var roads = response.data.roads;
        for(var i=0;i<roads.length;i++) {
          $rootScope.col_tab_array.push({
            from_name: roads[i].name,
            altitude: roads[i].altitude,
            length: roads[i].length,
            avggradient: roads[i].avggradient,
            segment: roads[i].segment,
            from_place: roads[i].FromPlace,
            to_place: roads[i].ToPlace,
            global_rating: roads[i].global_rating,
            user_rating: roads[i].user_rating,
            segment_id: roads[i].segment.split('segments/')[1],
            isActive: false
          });
          if(i==0) {
            $rootScope.col_tab_array[i].isActive = true;
            $rootScope.activeFrom = $rootScope.col_tab_array[i];
            if($rootScope.activeFrom.user_rating != null){
              $scope.segment_rating = $rootScope.activeFrom.user_rating;
            }
            else
              $scope.segment_rating = 0;
            //$scope.loadSameSegmentUsers();
            loadLastFiveCompared(false);
          }
        }
        $scope.initMap();
        //Console.log("OUR TABS --------------------------------"+JSON.stringify($rootScope.col_tab_array));
        var Proj = $rootScope.recalculate(parseFloat($rootScope.activeFrom.avggradient.split('%')[0]), $rootScope.profile_data.ftp, parseFloat($rootScope.activeFrom.length.split(' ')[0])*1000,
          parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight));
          //$rootScope.activeFrom.current_user_segment_time = 'You haven\'t done this segment yet.';
          updateCurrentTabTime(); 
          $rootScope.activeFrom.Projected_segment_time = new Date(Proj.time*1000);
      });
    }

    $scope.GoBack = function() {
      $ionicHistory.goBack();
    }
    $scope.redirectToStrava = function(){
      $window.open($rootScope.activeFrom.segment, '_blank');
    };

    function loadLastFiveCompared(isSelected) {
      $scope.members_to_compare = [];
      var link = "https://www.deuxmille.cc/api/user/get_last_five_users/?insecure=cool&user_id="+$rootScope.userID;
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var users = response.data.users;
          for(var i=0;i<users.length;i++) {
            if(Array.isArray(users[i].ftp))
              users[i].ftp = (users[i].ftp)[0];
            if(Array.isArray(users[i].ftp))
              users[i].weight = (users[i].weight)[0];
            if(Array.isArray(users[i].bike))
              users[i].ftp = (users[i].ftp)[0];
            $scope.members_to_compare.push({
              user_id: users[i].user_id,
              display_name: users[i].display_name,
              time: new Date(users[i].time*1000),
              avatar: users[i].avatar,
              ftp: users[i].ftp,
              weight: users[i].weight,
              bike_weight: users[i].bike_weight,
              isChecked: false
            });
          }
          if(isSelected) {
            $scope.addToCompareUsers($scope.members_to_compare[0]);
          }
        });
    }
    
    $scope.ClickCompareMembers = function() {
      $scope.isCompareMembers = !$scope.isCompareMembers;
      if($scope.isCompareMembers){
        loadAllUsers();
        return;
      }
    }

    function updateCurrentTabTime() {
      $rootScope.activeFrom.current_user_segment_time = 'You haven\'t done this segment yet.';
      var link = "https://www.deuxmille.cc/api/user/get_user_road_time/?insecure=cool&segment_id="+$rootScope.activeFrom.segment_id+"&user_id="+$rootScope.userID;
      //console.log(link);
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data != null) {
            if(usercols[i].time != 0)
              $rootScope.activeFrom.current_user_segment_time = new Date(parseInt(response.data)*1000);
            else
              $rootScope.activeFrom.current_user_segment_time = 'You have done this segment';
          }
      });
    }

    $scope.ChangeActive = function(item) {
      $scope.checked_members = [];
      for(var i=0;i<$rootScope.col_tab_array.length;i++)
        $rootScope.col_tab_array[i].isActive = false;
      item.isActive = true;
      $rootScope.activeFrom = item;
      updateCurrentTabTime();
      $scope.initMap();
      if($rootScope.activeFrom.user_rating != null){
        $scope.segment_rating = $rootScope.activeFrom.user_rating;
      } else{
        $scope.segment_rating = 0;
      }
      //Console.log(JSON.stringify($rootScope.activeFrom));
      var Proj = $rootScope.recalculate(parseFloat($rootScope.activeFrom.avggradient.split('%')[0]), $rootScope.profile_data.ftp, parseFloat($rootScope.activeFrom.length.split(' ')[0])*1000,
        parseFloat($rootScope.profile_data.weight) + parseFloat($rootScope.profile_data.bikeweight));
        $rootScope.activeFrom.Projected_segment_time = new Date(Proj.time*1000);
    }
    /*$scope.loadSameSegmentUsers = function(){
      $rootScope.members_to_compare = [];
      var cur_segment_id = $rootScope.activeFrom.segment;
      cur_segment_id = cur_segment_id.split("segments/")[1];
      link = "https://www.deuxmille.cc/api/user/get_samecol_users/?segment_id="+cur_segment_id+"&insecure=cool";
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var users = response.data.users;
          for(var i=0;i<users.length;i++) {
            if(users[i].user_id == $rootScope.userID)
            $rootScope.activeFrom.current_user_segment_time = new Date(users[i].time*1000);
            $scope.members_to_compare.push({
              user_id: users[i].user_id,
              display_name: users[i].display_name,
              time: new Date(users[i].time*1000),
              avatar: users[i].avatar,
              ftp: users[i].ftp,
              weight: users[i].weight,
              bike_weight: users[i].bike_weight,
              isChecked: false
            });
            $scope.members_to_compare[i].speed = parseInt(parseFloat($rootScope.activeFrom.length.split(' ')[0]) / ($scope.members_to_compare[i].time.getTime()/3600000));
          }
        });
    }*/
    function loadAllUsers() {
      $scope.all_members = [];
      var link = "https://www.deuxmille.cc/api/user/get_all_users/?insecure=cool&user_id="+$rootScope.userID;
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var users = response.data.users;
          for(var i=0;i<users.length;i++) {
            $scope.all_members.push({
              user_id: users[i].id,
              display_name: users[i].display_name,
              //time: new Date(users[i].time*1000),
              avatar: users[i].avatar,
              //ftp: users[i].ftp,
              //weight: users[i].weight,
              //bike_weight: users[i].bike_weight,
              isChecked: false
            });
          }
        });
      //$scope.members_to_compare = [];
    }
    $scope.initMap = function() {
      var directionsDisplay = new google.maps.DirectionsRenderer();
      var directionsService = new google.maps.DirectionsService();
      var FromLat = $rootScope.activeFrom.from_place;
      var FromLng = $rootScope.activeFrom.from_place;
      var ToLat = $rootScope.activeFrom.to_place;
      var ToLng = $rootScope.activeFrom.to_place;

      FromLat = FromLat.split('lat"')[1];
      FromLat = FromLat.split('"')[1];
      FromLat = parseFloat(FromLat.split('"')[0]);
      FromLng = FromLng.split('lng"')[1];
      FromLng = FromLng.split('"')[1];
      FromLng = parseFloat(FromLng.split('"')[0]);

      ToLat = ToLat.split('lat"')[1];
      ToLat = ToLat.split('"')[1];
      ToLat = parseFloat(ToLat.split('"')[0]);
      ToLng = ToLng.split('lng"')[1];
      ToLng = ToLng.split('"')[1];
      ToLng = parseFloat(ToLng.split('"')[0]);

      var MapCenter = {lat: (FromLat+ToLat)/2, lng: (FromLng+ToLng)/2};
      var map = new google.maps.Map(document.getElementsByClassName('map_block')[0], {
        center: MapCenter,
        zoom: 11,
        disableDefaultUI: true,
        zoomControl:true
      });
      var markerFrom = new google.maps.Marker({
        position: {lat: parseFloat(ToLat), lng: parseFloat(ToLng)},
        map: map,
        title: 'Hello World!'
      });
      var markerTo = new google.maps.Marker({
        position: {lat: parseFloat(FromLat), lng: parseFloat(FromLng)},
        map: map,
        title: 'Hello World!'
      });
      var start = new google.maps.LatLng(parseFloat(FromLat), parseFloat(FromLng));
      var end = new google.maps.LatLng(parseFloat(ToLat), parseFloat(ToLng));
      directionsDisplay.setMap(map);
      var bounds = new google.maps.LatLngBounds();
      bounds.extend(start);
      bounds.extend(end);
      map.fitBounds(bounds);
      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.WALKING
      };
      directionsService.route(request, function (response) {
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(map);
      });
    }

    $scope.changeIsChecked = function(item){
      for(var i = 0;i<$scope.all_members.length;i++) {
        if($scope.all_members[i].isChecked) {
          $scope.all_members[i].isChecked = false;
        }
      }
      item.isChecked = true;
    }

    $scope.addToCompareUsers = function(item) {
      for(var i = 0;i<$scope.all_members.length;i++) {
        if($scope.all_members[i].isChecked) {
          $scope.all_members[i].isChecked = false;
        }
      }
      item.isChecked = true;
      $scope.Compare_member.CheckForComparing = true;
      var Proj = $rootScope.recalculate(parseFloat($rootScope.activeFrom.avggradient.split('%')[0]), item.ftp, parseFloat($rootScope.activeFrom.length.split(' ')[0])*1000,
          parseFloat(item.weight) + parseFloat(item.bike_weight));
      $scope.Compare_member.ETime = new Date(Proj.time*1000);
      $scope.Compare_member.avatar = item.avatar;
      $scope.Compare_member.display_name = item.display_name;
        //$scope.checked_members.push(item);
        //$scope.Compare_member = item.;
    }
    $scope.loadCheckedUsers = function() {
      for(var i = 0;i<$scope.all_members.length;i++) {
        if($scope.all_members[i].isChecked) {
          var link = "https://www.deuxmille.cc/api/user/update_users_compare_members/?insecure=cool&user_id="+$rootScope.userID+"&last_user_id="+$scope.all_members[i].user_id;
          homeFactory
            .colsCall(link, 'GET')
            .then(function (response) {
              loadLastFiveCompared(true);
              $scope.isCompareMembers = !$scope.isCompareMembers;
            },
            function(error) {
              alert('Something went wrong');
            });
          break;
        }
      }
    }

    $scope.changeRating = function(count_of_stars) {
      link = "https://www.deuxmille.cc/api/user/rate_segment/?user_id="+$rootScope.userID+"&segment_id="+$rootScope.activeFrom.segment.split("segments/")[1]+
        "&rating="+count_of_stars+"&insecure=cool";
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          $scope.segment_rating = parseInt(count_of_stars);
          $rootScope.activeFrom.user_rating = parseInt(count_of_stars);
          var link2 = "https://www.deuxmille.cc/api/user/get_average_rate_segment/?segment_id="+$rootScope.activeFrom.segment.split("segments/")[1]+
          "&insecure=cool";
          homeFactory
            .colsCall(link2, 'GET')
            .then(function (response2) {
              //Console.log(JSON.stringify(response2));
              if(response2.data.rating == null)
                $rootScope.activeFrom.global_rating = 0;
              else
                $rootScope.activeFrom.global_rating = parseFloat(response2.data.rating);
            });
        });

    }
    $scope.isYellow = function(count_of_start) {
      if(parseInt(count_of_start) <= $scope.segment_rating)
        return true;
      return false;
    }
    $scope.goToSaveCol = function() {
      $state.go('save_col', {
        col_id: $stateParams.col_id,
        segments: $rootScope.col_tab_array,
        col_name: $scope.current_col_name
      });
    }

  })

  .controller('SaveColCtrl', function ($scope, $state, $stateParams, $ionicHistory, $rootScope, $window, $cordovaDatePicker, homeFactory) {
    $scope.goBack = function() {
      $ionicHistory.goBack();
    };
    
    $scope.lengthOfElapsedTime = 0;
    $scope.IsSuccesfull=0;
    $scope.errorDate = false;
    $scope.errorTime = false;
    $scope.NoPhoto = false;
    $scope.colName = $stateParams.col_name;
    $scope.segments = $stateParams.segments;
    //$rootScope.LinkToImgNewPost = "";

    $scope.Post = {
      col_id: $stateParams.col_id,
      userId: $rootScope.userID,
      date: new Date(),
      selectedSegment: $scope.segments[0],
      ElapsedTime: "",
      photo:""
    };

    $scope.InputTime = function() {
      if($scope.Post.ElapsedTime.length == 2 && $scope.Post.ElapsedTime.length > $scope.lengthOfElapsedTime)
        $scope.Post.ElapsedTime += ':';
      if($scope.Post.ElapsedTime.length == 3 && $scope.Post.ElapsedTime.length < $scope.lengthOfElapsedTime)
        $scope.Post.ElapsedTime = $scope.Post.ElapsedTime.substring(0, str.length - 1);
      if($scope.Post.ElapsedTime.length == 5 && $scope.Post.ElapsedTime.length > $scope.lengthOfElapsedTime)
        $scope.Post.ElapsedTime += ':';
      if($scope.Post.ElapsedTime.length == 6 && $scope.Post.ElapsedTime.length < $scope.lengthOfElapsedTime)
        $scope.Post.ElapsedTime = $scope.Post.ElapsedTime.substring(0, str.length - 1);
      $scope.lengthOfElapsedTime = $scope.Post.ElapsedTime.length;
    }

    $scope.SetImage = function() {
      $rootScope.isNewPost_Add.Add = true;
      $rootScope.isNewPost_Add.ColId = $stateParams.col_id;
      $state.go('album', {isNewPost: true});
    }

    $scope.ChangeDateTime = function() {
      var options = {
        date: $scope.Post.date,
        mode: 'date', // or 'time'
        allowOldDates: true,
        allowFutureDates: true,
        maxDate: new Date(),
        minDate: new Date(1970),
        doneButtonLabel: 'DONE',
        doneButtonColor: '#000000',
        cancelButtonLabel: 'CANCEL',
        cancelButtonColor: '#000000'
      };

      $cordovaDatePicker.show(options).then(function(date){
        $scope.Post.date = date;
        if($scope.Post.date == null)
          $scope.Post.date = new Date();
      }, function() {
        $scope.Post.date = new Date();
      });
    }

    $scope.SaveCol = function() {
      $rootScope.show();
      $scope.errorDate = false;
      $scope.errorTime = false;
      $scope.NoPhoto = false;
      $scope.Post.photo = $rootScope.LinkToImgNewPostNewPost;
      var Today = new Date();
      if($scope.Post.date.getTime() > Today.getTime()) {
        $rootScope.hide();
        $scope.errorDate = true;
        return;
      }
      var times = $scope.Post.ElapsedTime.split(':');
      if(times.length != 3) {
        $rootScope.hide();
        $scope.errorTime = true;
        return;
      }
      for(var i=0;i<times.length;i++) {
        if(isNaN(times[i])) {
          $rootScope.hide();
          $scope.errorTime = true;
          return;
        }
      }
      if((parseInt(times[1]) > 59) || (parseInt(times[2]) > 59)
           || (times[1].length != 2) || (times[2].length != 2)) {
        $rootScope.hide();
        $scope.errorTime = true;
        return;
      }
      var Time = parseInt(times[0])*3600 + parseInt(times[1])*60 + parseInt(times[2]);
      //var utcData = Math.floor(($scope.Post.date.getTime() + $scope.Post.date.getTimezoneOffset() * 60000)/1000);
      var utcData = $scope.Post.date.getTime()/1000;
      var link = "https://www.deuxmille.cc/api/posts/add_new_post/?u_id="+$rootScope.userID+"&s_id="+$scope.Post.selectedSegment.segment.split("segments/")[1]+
      "&c_id="+$scope.Post.col_id+"&e_id=0&edate="+utcData+"&etime="+Time+
      "&insecure=cool";
      if(($scope.Post.photo != "") && ($scope.Post.photo != null)) {
        link += "&image="+encodeURI($scope.Post.photo);
      }
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          if(response.data.status == 'success') {
            $ionicHistory.clearCache(['tab.cols', 'all_cols_page']);
            $scope.IsSuccesfull = 1;
          }
          else
            $scope.IsSuccesfull = 2;
          $rootScope.hide();
        });
    }

    $scope.isSuccess = function(num) {
      if (num == $scope.IsSuccesfull)
        return false;
      return true;
    }
//https://www.deuxmille.cc/api/posts/add_new_post/?u_id=316&s_id=1613885&c_id=91&e_id=0&edate=1520077719&etime=3309&insecure=cool
  })

  .controller('AllColsPageCtrl', function ($scope, $state, $stateParams, $ionicHistory, $rootScope, $compile, $window, homeFactory) {
    var markers = [];
    $scope.SelectedOnMapCol = {};
    $scope.all_cols_on_map = [];
    var infowindow = new google.maps.InfoWindow({
      content: 'bla'
  });
    $scope.goToColDetail = function(item) {
      $state.go('col_detail', {col_id: item.col_id, isChecked: item.isVisited});
    }
      LoadMap();

      function LoadMap() {
      $scope.all_cols_on_map = [];
      var link = "https://www.deuxmille.cc/api/posts/get_cols_coords/?user_id=" + $rootScope.userID;
      homeFactory
        .colsCall(link, 'GET')
        .then(function (response) {
          var MapCenter = {lat: 50.0, lng: 10.0};
          var map = new google.maps.Map(document.getElementById('all_cols_mappppp'), {
            center: MapCenter,
            zoom: 5,
            disableDefaultUI: true,
            zoomControl:true,
            zoomControlOptions: {
              position: google.maps.ControlPosition.RIGHT_TOP
            }
          });
          var cols = response.data.cols;
          for(var i=0;i<cols.length;i++) {
            if(cols[i].coords == null)
              continue;
            var name = cols[i].title;
            var height = cols[i].title;
            var isChecked = false;
            var possibleName = cols[i].title.substring(0, cols[i].title.indexOf(",") - 3);
            if(possibleName != "") {
              name = possibleName;
              height = height.split('(')[1];
              height = height.substring(0, height.length - 1);
            }
            $scope.all_cols_on_map.push({
              col_id: cols[i].post_id,
              col_name: name,
              col_height: height,
              isVisited: cols[i].visited,
              image: cols[i].image
            });
            var ToLat = cols[i].coords;
            var ToLng = cols[i].coords;
            ToLat = ToLat.split('lat"')[1];
            ToLat = ToLat.split('"')[1];
            ToLat = parseFloat(ToLat.split('"')[0]);
            ToLng = ToLng.split('lng"')[1];
            ToLng = ToLng.split('"')[1];
            ToLng = parseFloat(ToLng.split('"')[0]);
            markers.push({
              marker: new google.maps.Marker({
                position: {lat: parseFloat(ToLat), lng: parseFloat(ToLng)},
                map: map,
                title: cols[i].title,
                Col: { col_id: cols[i].post_id,
                       isVisited: cols[i].visited }
              }),
            });
            if(cols[i].visited) {
              markers[markers.length - 1].marker.setIcon("http://maps.google.com/mapfiles/ms/icons/yellow-dot.png");
            }//$state.go('col_detail', {col_id: item.col_id, isChecked: item.isVisited});
            markers[markers.length - 1].marker.addListener('click', function() {
              $scope.SelectedOnMapCol = this.Col;
              var compiled = $compile('<span ng-click="goToColDetail(SelectedOnMapCol)">'+this.title+'</span>')($scope);
              infowindow.setContent(compiled[0]);
              infowindow.open(map, this);
            });
          }
        });
    }
    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

  })

  .controller('ShopCtrl', function ($scope, $state, $stateParams, $ionicHistory, $rootScope, $window, homeFactory) {
    $rootScope.shop_items = [];
    $rootScope.user_open_id = $rootScope.userID;
    initShopItems();

    function initShopItems() {
      /*for(var i=0;i<9;i++)
      $scope.shop_items.push({
        id: i,
        name: "Supper Cross Jersey",
        price: parseFloat(130.01),
        photo: "img/coat.png"
      });*/
      var link = 'https://www.deuxmille.cc/wp-json/wc/v2/products';
      homeFactory.shopAPI(link, 'GET').then(function (response) {
        for(var i=0;i<response.data.length;i++) {
          var item = response.data[i];
          var size = [];
          if(response.data[i].attributes[0] != null)
            if(response.data[i].attributes[0].name == "Size")
              size = response.data[i].attributes[0].options;
          $rootScope.shop_items.push({
            id: item.id,
            name: item.name,
            price: item.price_html,
            photo: item.images[0].src,//"img/coat.png"
            similarItemsIds:response.data[i].related_ids,
            sizes:size,
            description: response.data[i].short_description,
            link: response.data[i].permalink
          });
        }
      });
    }

    $scope.ClickItem = function(item) {
      $rootScope.selectedShopProduct = item;
      $state.go('product_description');
    }
    $scope.GoToCard = function() {
      $state.go('cart');
    }
  })

  .controller('ProductDescCtrl', function ($scope, $state, $stateParams, $ionicScrollDelegate, $ionicHistory, $rootScope, $window, homeFactory) {
    $scope.SimilarItems = [];
      $scope.SimilarItems = [];
      for(var i=0;i<$rootScope.selectedShopProduct.similarItemsIds.length;i++) {
        for(var j=0;j<$rootScope.shop_items.length;j++) {
          if($rootScope.shop_items[j].id == $rootScope.selectedShopProduct.similarItemsIds[i])
            $scope.SimilarItems.push($rootScope.shop_items[j]);
        }
      }

    $scope.CheckSize = function(sizeStr) {
      for(var i=0;i<$rootScope.selectedShopProduct.sizes.length;i++)
        if($rootScope.selectedShopProduct.sizes[i] == sizeStr)
          return true;
      return false;
    }

    $scope.ViewOnSite = function() {
        $window.open($rootScope.selectedShopProduct.link, '_blank');
    }

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }
    $scope.GoToCard = function() {
      $state.go('cart');
    }
    $scope.ClickItem = function(item) {
      $rootScope.selectedShopProduct = item;
      $scope.SimilarItems = [];
      for(var i=0;i<$rootScope.selectedShopProduct.similarItemsIds.length;i++) {
        for(var j=0;j<$rootScope.shop_items.length;j++) {
          if($rootScope.shop_items[j].id == $rootScope.selectedShopProduct.similarItemsIds[i])
            $scope.SimilarItems.push($rootScope.shop_items[j]);
        }
      }
      $ionicScrollDelegate.scrollTop();
    }
  })

  .controller('CartCtrl', function ($scope, $state, $stateParams, $ionicHistory, $rootScope, $window, homeFactory) {

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }
    $scope.CheckOutSum = function() {
      $state.go('checkout_sum');
    }
  })

  .controller('CheckOutCtrl', function ($scope, $state, $stateParams, $ionicHistory, $rootScope, $window, homeFactory) {

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }
  });

/*C:\Program Files\Java\jdk1.8.0_161\bin>keytool -exportcert -alias androiddebugkey
-keystore C:/Users/Yuri/.android/debug.keystore | "C:\OpenSSL\bin\openssl" sha1 -binary |
"C:\OpenSSL\bin\openssl" base64*/
