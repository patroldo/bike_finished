
$( document ).ready(function () {
  setIndexHeight();
  setTopPosition();

  $('.not_item').click(function () {
    $( this ).toggleClass('active');
  })

});


$( window ).resize(function () {

  setIndexHeight();
  setTopPosition();


});

function setIndexHeight() {

  var indexHeight = $(window).height();
  $('.index_wrapper').css('height', indexHeight);
  if (indexHeight > 620) {
    $('.registration_block, .login_block').css('height', indexHeight);
  } else {
    $('.registration_block, .login_block').css('height', 640);
  }
}

function setTopPosition() {
  var indexHeight = $(window).height();
  if (indexHeight > 620) {
    var blockTopPosition = (indexHeight - $('.reg_block_outer').height())/2;
    $('.reg_block_outer').css('top',blockTopPosition);
  } else {
    $('.reg_block_outer').css('top', 110);
  }

}
