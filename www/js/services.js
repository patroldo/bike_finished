angular.module('starter.services', [])

.factory('homeFactory', function ($http, $rootScope, $timeout) {
  return {
    facebookCall: function(link, type){
      return $http({
        method: type,
        url: link,
        headers:{
            'Content-Type': 'application/json'
        },
        timeout: 25000
      }).success(function(response){
        return response;
      }).error(function(error){
        return error;
      });
    },
    colsCall: function (link, type) {
      return $http({
        method: type,
        url: link,
        headers:{
            'Content-Type': 'application/json'
        },
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    },
    apiCall: function (link, type) {
      return $http({
        method: type,
        url: link,
        headers:{
            'Content-Type': 'application/json'
        },
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    },
    stravaAPI: function (link, type, token) {
      var auth = 'Bearer '+token;
      return $http({
        method: type,
        url: link,
        headers:{
            'Content-Type': 'application/json',
            'Authorization': auth
        },
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    },
    updateStravaTokens: function (link, type, data) {
      return $http({
        method: type,
        url: link,
        data: data,
        headers:{
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    },
    shopAPI: function (link, type) {
      return $http({
        method: type,
        url: link,
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Basic Y2tfOWE2ZjNjYWZmZmIzMDY5NzQxZWM0YTc5NjVhNWM3Y2NkOGIyYmE4Nzpjc19iMTQ5OTRjY2U2MTA0OWUwNjI4MjZiZWRjZjlmOGE5NjJjMzM5OWVk'
        },
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    },
    postImageCall: function (link, type, dataa) {
      return $http({
        method: type,
        url: link,
        headers:{
          //'Content-Type': 'image/png'
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: dataa,
        timeout: 25000
      }).success(function (response) {
        return response;
      }).error(function (error) {
        return error;
      })
    }
  }
});
